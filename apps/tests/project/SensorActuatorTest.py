'''
Created on Apr 13, 2020

@author: HarshithaGS
'''

import unittest
import json
from project import SensorData
from project import ActuatorData 
from project import DataUtil
"""
Test class for all requisite Module12 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class SensorActuatorTest(unittest.TestCase):

    """
    Use this to setup your tests. This is where you may want to load configuration
    information (if needed), initialize class-scoped variables, create class-scoped
    instances of complex objects, initialize any requisite connections, etc.
    """
    def setUp(self):
        self.sensor=SensorData.SensorData()
        self.actuator=ActuatorData.ActuatorData()
        self.DataUtil=DataUtil.DataUtil()
        pass

    """
    Use this to tear down any allocated resources after your tests are complete. This
    is where you may want to release connections, zero out any long-term data, etc.
    """
    def tearDown(self):
        del self.sensor
        del self.actuator
        pass

    """
    Place your comments describing the test here.
    """
    '''This method checks  if the add value function in sensor data is correctly functioning'''
    def testAddValue(self):
        v=[20.00,31.27,35.35]
        self.assertTrue(type(self.sensor.addValue(v) is object))
    '''This function tests the get pressure method'''    
    def testgetpresValue(self):    
        v1=[25.00,32.57,32.36]
        self.sensor.addValue(v1)    
        print(self.sensor.getpresValue())
        self.assertTrue(type(self.sensor.getpresValue() is float))
    ''' THsi method test the  updation of the actuated data'''   
    def testgetupdatepresvalue(self):
        data={"value": 1025.0, "timestamp": 1587139070376, "context": {}, "created_at": 1587139070376}
        new=json.dumps(data)
        newVal=self.DataUtil.updateActuatorData("pressureactuator",new)
        print (newVal)
        self.actuator.updateData(newVal)
        received=self.actuator.getPressureActuator()
        self.assertTrue(type(self.actuator.getPressureActuator() is float))
        self.assertEquals(1025.0, received, "Passed value is not matching")

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()