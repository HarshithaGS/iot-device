import unittest

from project import SmtpClientConnector
from project import DataUtil
from project import SensorData
from project import HTTPClientPublisher
from project import MqttClientConnector
from datetime import datetime
from project import MqttSubClient

"""
Test class for all requisite Project functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""

UBIDOTS_DEVICE_LABEL = "healthcare"
UBIDOTS_VARIABLE_TEMPERATURE_LABEL = "tempactuator"
UBIDOTS_VARIABLE_PRESSURE_LABEL = "pressureactuator"
UBIDOTS_VARIABLE_HUMIDITY_LABEL = "humidityactuator"
UBIDOTS_VARIABLES = [UBIDOTS_VARIABLE_TEMPERATURE_LABEL, UBIDOTS_VARIABLE_PRESSURE_LABEL, UBIDOTS_VARIABLE_HUMIDITY_LABEL]

class ProjectTest(unittest.TestCase):
	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.smtp=SmtpClientConnector.SmtpClientConnector()
		self.DataUtil=DataUtil.DataUtil()
		self.SensorData=SensorData.SensorData()
		self.HTTPpublisher=HTTPClientPublisher.HTTPClientPublisher()
		self.mqttconnector=MqttClientConnector.MqttClientConnector()
		self.MQTTsubscriber = MqttSubClient.MqttSubClient()

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.smtp
		
		del self.SensorData
		del self.HTTPpublisher
		del self.mqttconnector
		del self.MQTTsubscriber

	"""
	Place your comments describing the test here.
	"""
	'''
	checks if the sensor data is set
	'''
	def setSensorData(self):
		self.timeStamp = str(datetime.now())
		self.temperature = self.SensorData.setTemperature(25)
		self.pressure = self.SensorData.setHumidity(34)
		self.humidity = self.SensorData.setPressure(1023)
		return self.SensorData
	''' Test if teh email notification issent ot teh correct address'''
	def testSMTPConnection(self):
		self.assertRaises(Exception,self.smtp.publishMessage("Temperature crossed threshold", "Value"))
		pass
	
	'''This method test the HTTP connection made between constrained device and Ubidots'''
	def testHTTPConnection(self):
		sensor=self.setSensorData()
		self.HTTPpublisher.publish(UBIDOTS_DEVICE_LABEL, self.DataUtil.toJsonFromSensorData(sensor))
		pass
	'''
	This method test the subscription done by the constrained device from java published via local MQTT'''
		
	def testMQTTSubscribe(self):
		self.MQTTsubscriber.connect()
		value=self.MQTTsubscriber.subscribe(UBIDOTS_VARIABLES)
		print(value)
		self.assertTrue(type(value is object))
		self.MQTTsubscriber.disconnect()
	'''CHceks the conversion of Sensordat ato JSON'''	
	def testSensorDataToJson(self):
		sensor=self.setSensorData()
		print(self.DataUtil.toJsonFromSensorData(sensor));
		

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()