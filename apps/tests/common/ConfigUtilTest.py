import unittest

from labs.common.ConfigUtil import ConfigUtil
from _overlapped import NULL


"""
Test class for the ConfigUtil module.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class ConfigUtilTest(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.configUtil = ConfigUtil()
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.configUtil	
		
	"""
	Tests retrieval of a boolean property.
	"""
	def testGetBooleanProperty(self):
		# = self.configUtil.getProperty(section, key)
		pass
	"""
	Tests retrieval of an integer property.
	"""
	def testGetIntegerProperty(self):
		# TODO: implement this
		pass
	
	"""
	Tests retrieval of a string property.
	"""
	def testGetProperty(self):
		pass
	
	"""
	Tests if a property exists.
	"""
	def testHasProperty(self):
		
		self.assertTrue(type(self.configUtil.getProperty("smtp.cloud", "port") is bool))		
		pass

	"""
	Tests if a section exists.
	"""
	def testHasSection(self):
		self.assertTrue(type(self.configUtil.getProperty("smtp.cloud", "port") is bool))		
		pass
	


	"""
	Tests if  config file is loaded - need to chnage def names
	"""
	def testhasConfigData(self):
		self.assertTrue(type(self.configUtil.loadConfig() is bool))
		pass
	
	"""
	Tests if  data is present in the config file  loaded
	"""
	def testConfigData(self):
		self.assertTrue(type(self.configUtil.getConfigData() is bool))	
		pass
		
		
	"""
	Tests if  section and key is right
	"""
	def testSectionData(self):
		self.assertTrue(type(self.configUtil.getProperty("smtp.cloud", "port") is str))		
		pass
	
if __name__ == "__main__":
	unittest.main()
