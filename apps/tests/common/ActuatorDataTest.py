import unittest

from labs.common import ActuatorData

"""
Test class for all requisite ActuatorData functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class ActuatorDataTest(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.actuatorDataTest = ActuatorData.ActuatorData()

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.actuatorDataTest
	
	"""
	+getCommand() : String. Returns the command as a String (e.g. �INCREASE TEMP�).
	"""
	def testgetCommand(self):
		self.actuatorDataTest.setCommand('1');
		received=self.actuatorDataTest.getCommand()
		self.assertTrue(type(self.actuatorDataTest.getCommand() is str))
		self.assertEquals("1", received, "Passed value is not matching")
	
	"""
	+getValue() : float. Returns the current value (most recently added) as a float (e.g. 0.0f).
	"""
	def testgetValue(self):
		self.actuatorDataTest.setValue(1.2);
		received=self.actuatorDataTest.getValue()
		self.assertTrue(type(self.actuatorDataTest.getValue() is float))
		self.assertEquals(3.6, received, "Passed value is not matching")
	"""
	+getName() : String. Returns the name of the sensor as a String (e.g. �Temperature�).
	"""
	def testgetName(self):
		self.actuatorDataTest.setName("Actuator decrease temp");
		received=self.actuatorDataTest.getName()
		self.assertTrue(type(self.actuatorDataTest.getName() is str))
		self.assertEquals("Actuator decrease temp", received, "Passed value is not matching")
		
	"""
	+setCommand(String). The first and only parameter represents the command as a String (e.g. �INCREASE TEMP�).
	"""	
	def testsetCommand(self):
		self.actuatorDataTest.setCommand("Decrease the temperature");
		self.assertEquals("Decrease the temperature", self.actuatorDataTest.command, "Passed value is not matching")
		
	
	'''
	+setName(String). The first and only parameter represents the name of the sensor as a String (e.g. �Temperature�).
	'''
	def testsetName(self):
		self.actuatorDataTest.setName("Actuator Decrease")
		self.assertEquals("Actuator Decrease", self.actuatorDataTest.name, "Passed value is not matching")
		
if __name__ == "__main__":
	unittest.main()
	


		
	
