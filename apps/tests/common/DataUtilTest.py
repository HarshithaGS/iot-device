import unittest
from labs.common import SensorData
from labs.common import ActuatorData
from labs.common import DataUtil
import json


"""
Test class for all requisite DataUtil functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class DataUtilTest(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.sensorData = SensorData.SensorData("Data")
		self.actuatorData = ActuatorData.ActuatorData()
		self.dataU = DataUtil.DataUtil()

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.actuatorData
		del self.sensorData
	
	#Testing if ActuatorData is written to Json
	def testActuatorDataToJson(self):
		adJson = self.dataU.toJsonFromActuatorData(self.actuatorData)
		print(adJson)
		
	#Testing if SensorData is written to Json
	def testSensorDataToJson(self):
		sdJson = self.dataU.toJsonFromSensorData(self.sensorData)
		print(sdJson)
		
if __name__ == "__main__":
	unittest.main()