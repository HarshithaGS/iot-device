import unittest
from labs.common.SensorData import *

"""
Test class for all requisite SensorData functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class SensorDataTest(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.SensorData = SensorData()

	
	"""
	The first and only parameter represents the float value to add to the sensor data (e.g. 0.0f, then 30.0f).
	"""
	def testaddValue(self):
		self.value = 15.2
		self.assertTrue(type(self.SensorData.addValue(self.value) is float))
		pass
	
	"""
	returns the average value (the count of all values divided by the aggregate value - e.g.15.0f).
	"""
	def testgetAverageValue(self):
		self.assertTrue(type(self.SensorData.getAvgValue() is float))
		pass
		
	"""
	Returns the total number of times addValue(float) is called (e.g. 2).
	"""
	def testgetcount(self):
		self.assertTrue(type(self.SensorData.samples is int))
		pass	
	
	"""
	float. Returns the current value (most recently added) as a float (e.g. 0.0f).(self):
	"""
	def testgetCurrentValue(self): 
		self.assertTrue(type(self.SensorData.samples is float))
		pass
	
	"""
	float. Returns the maximum value (most recently added) as a float (e.g. 0.0f).(self):
	"""
	def testgetMaxValue(self): 
		self.assertTrue(type(self.SensorData.getMaxValue() is float))
		pass
	
	"""
	float. Returns the minimum value (most recently added) as a float (e.g. 0.0f).(self):
	"""
	def testgetMinValue(self): 
		self.assertTrue(type(self.SensorData.getMinValue() is float))
		pass
	
	"""
	float. Returns the name of the sensor as a string:
	"""
	def testgetName(self): 
		self.assertTrue(type(self.SensorData.name is str))
		pass
	
	"""
	temp
	"""
	def testsetName(self): 
		self.assertTrue(type(self.SensorData.setName("Temp") is str))
		pass	
	
	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.SensorData
	
	
if __name__ == "__main__":
	unittest.main()