import unittest

from labs.module02 import  SmtpClientConnector, TempSensorEmulatorTask


"""
Test class for all requisite Module02 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""


class Module02Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""

	def setUp(self):
		self.TempSE = TempSensorEmulatorTask.TempSensorEmulatorTask(5)
		self.Smtp   = SmtpClientConnector.SmtpClientConnector()
		
	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""

	def tearDown(self):
		del self.TempSE
		del self.Smtp
		
	
	"""
	Place your comments describing the test here.
	"""
	
	"""
	This definition will return a reference to the SensorData. The getSensorData is the method which stores the temp data  
	"""
	def testTempSensorEmulatorTest(self):
 		self.assertTrue(type(self.TempSE.getSensorData() is str))

	"""
	Base - case Exception
	This method tests the base case of sending the email. Basically given the correct address , port and host name and other network data in the config file This should send a mail saying hi ,wassup
	"""	
	def testSmtpClientConnector(self):
		self.assertEqual(None, self.Smtp.publishMessage("hey", "test mail"), "Error");
	
# 		
		
if __name__ == "__main__":
	# import sys;sys.argv = ['', 'Test.testName']
	unittest.main()
