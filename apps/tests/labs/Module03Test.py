import unittest
from labs.module03 import TempActuatorAdaptor
from labs.common import ActuatorData
from labs.common import SensorData
from labs.module03 import DeviceDataManager


"""
Test class for all requisite Module03 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class Module03Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.tempactuator=TempActuatorAdaptor.TempActuatorAdaptor()
		self.deviceM=DeviceDataManager.DeviceDataManager()
		self.sensordata=SensorData.SensorData()
		self.sensordata.addValue(1)
		self.actuator=ActuatorData.ActuatorData()
		self.actuator.val=0.5

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.tempactuator
		del self.deviceM
		del self.sensordata
		del self.actuator
		
	"""
	+updateActuator(ActuatorData) : boolean. The first and only parameter represents the ActuatorData instance. Returns
	true on successful actuation; false otherwise.
	"""
	def testupdateActuator(self):
		var=self.tempactuator.sensehatMessage(self.actuator)
		self.assertEquals(True, var, "Passed value is not matching")
		
	'''
	+handleDeviceDataManager(SensorData) : ActuatorData. Callback function that will get invoked - passing the SensorData
	instance - whenever the TempSensorAdaptorTask updates SensorData with new information.
	'''
	def testhandleDeviceDataManager(self):
		var=self.deviceM.receive(self.sensordata, 4)
		self.assertTrue(type(var is object))

if __name__ == "__main__":
	unittest.main()
	
