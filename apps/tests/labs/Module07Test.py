import unittest
from labs.module07.TempSensorAdaptorTask import TempSensorAdaptorTask
from labs.common.SensorData import SensorData
from labs.module07 import CoapClientConnector

from labs.common.DataUtil import DataUtil
from labs.common import ConfigUtil

"""
Test class for all requisite Module07 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class Module07Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		#call instances of all classes
		self.SensorData=SensorData("data")
		self.datautil=DataUtil()
		self.config = ConfigUtil.ConfigUtil()
		self.coapHost = (self.config.getProperty(self.config.configConst.COAP_DEVICE_SECTION, self.config.configConst.COAP_HOST))
		self.coapPort = int(self.config.getProperty(self.config.configConst.COAP_DEVICE_SECTION, self.config.configConst.COAP_PORT))
		self.coapPath = 'Temperature'
		self.coapClient = CoapClientConnector.CoapClientConnector(self.coapHost, self.coapPort, self.coapPath)
		#ping request
		self.coapClient.ping()
		self.coapClient.get()
		

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.coapClient

	"""
	Place your comments describing the test here.
	"""
	def testPostMethod(self):
		self.SensorData.addValue(5)
		self.json_data = self.datautil.toJsonFromSensorData(self.SensorData)
		print(self.json_data)
		#call post method to post the json data to coap broker	
		var=self.coapClient.post(self.json_data)
		print(var)
		self.assertTrue(type(var is object))
		
		
if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()