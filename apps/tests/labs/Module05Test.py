import unittest
from labs.module05 import MultiActuatorAdaptor
from labs.common import ActuatorData
from labs.common import SensorData
from labs.module05 import DeviceDataManager
from labs.common import PersistenceUtil


"""
Test class for all requisite Module05 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class Module05Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.multi_actuator=MultiActuatorAdaptor.MultiActuatorAdaptor()
		self.device_manager=DeviceDataManager.DeviceDataManager()
		self.SensorData=SensorData.SensorData("Data")
		self.actuatorData=ActuatorData.ActuatorData()
		self.actuatorData.val=13.2
		self.persistence=PersistenceUtil.PersistenceUtil()

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.multi_actuator

	"""
	Place your comments describing the test here.
	"""
	# returns True if an ACtuator Data is updated
	def testupdateActuatorData(self):
		var=self.multi_actuator.sensehatMessage(self.actuatorData)
		self.assertEquals(True, var, "Passed Value not matching")
	
	# Returns the number of data read from The Redis DB
	def testreadActuatorDatafromRedis(self):
		var=self.device_manager.ManageData()
		print(var)
		self.assertEquals(12, var, "Passed Value not matching")
	
	#Prints a statement once all the Data is written into the Redis DB
	def testsendSensorDatatoRedis(self):
		var=self.persistence.writeToRedis(self.SensorData, "Humidity Reading is")
		print(var)
		self.assertEquals("Data is set to Redis", var, "Passed Value not matching")
	

if __name__ == "__main__":
	unittest.main()
	
