import unittest
from labs.module04 import MultiActuatorAdaptor
from labs.common import ActuatorData
from labs.common import SensorData

"""
Test class for all requisite Module04 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class Module04Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.multiactuator=MultiActuatorAdaptor.MultiActuatorAdaptor()
		self.sensordata=SensorData.SensorData("Alert")
		self.sensordata.addValue(1)
		self.actuator=ActuatorData.ActuatorData()
		self.actuator.val=0.5

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.mutliactuator
		del self.sensordata
		del self.actuator

	"""
	+updateActuator(ActuatorData) : boolean. The first and only parameter represents the ActuatorData instance. Returns
	true on successful actuation; false otherwise.
	"""
	def testupdateActuator(self):
		var=self.multiactuator.sensehatMessage(self.actuator)
		self.assertEquals(True, var, "Passed value is not matching")

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()