import unittest

from labs.module06 import MqttClientConnector
"""
Test class for all requisite Module06 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
from labs.common.SensorData import SensorData
from labs.common.ConfigConst import ConfigConst
from labs.module06 import TempSensorAdaptorTask
from labs.module06.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask
from labs.module06 import MqttClientConnector


class Module06Test(unittest.TestCase):
	
	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.mqttcc = MqttClientConnector.MqttClientConnector()
		self.data = SensorData
		self.host = ConfigConst.MQTT_HOST
		self.temp = TempSensorAdaptorTask.TempSensorAdaptorTask(0.5,'Temp')
		self.humidity = HumiditySensorAdaptorTask(0.5, 'Humidity')
		self.connect = MqttClientConnector.MqttClientConnector()
		self.SensorData=SensorData("data")


	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		del self.temp
		del self.humidity
		del self.connect

	def testTempTask(self):
		self.assertEquals(True , self.temp.run())
 		
	def testHumidityTask(self):
		self.assertEquals(True , self.temp.run())

	def connect(self):
		var=self.connect.publish('test', self.SensorData._str_(), "mqtt.eclipse.org", 1)
		print(var)
		self.assertTrue(type(var is object))
		
if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()