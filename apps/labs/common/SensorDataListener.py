'''
Created on Feb 20, 2020

@author: HarshithaGS
'''
'''
This class listens to the Redis as once the Sensor Data is read from PI and stored in Redis , it prints a statement to notify to go to Gateway
'''
class SensorDataListener(object):
    
    #constructor
    def __init__(self):
        pass
 
    
    def onMessage(self):
        print("Go to Gateway...")