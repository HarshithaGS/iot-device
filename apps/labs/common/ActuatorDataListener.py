'''
Created on Feb 20, 2020

@author: HarshithaGS
'''
#import
from labs.common import PersistenceUtil

'''
This class listens to the Redis  and returns a message whenever it redis has a new Actuator Data updated into the Redis.
'''
class ActuatorDataListener(object):
    #constructor
    def __init__(self):
        pass
     
    '''
    The onMessage Def will print a statement whenver it reads a new Actuator Data from the Redis
    '''    
    def onMessage(self):
        self.persistence=PersistenceUtil.PersistenceUtil()
        if(self.persistence.flag==1):
            print("New message read")
        

        