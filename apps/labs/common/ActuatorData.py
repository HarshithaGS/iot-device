'''
Created on Feb 6, 2020

@author: HarshithaGS
'''

import os #importing os
from datetime import datetime #importing datetime to perform date and time operations

#static/defined variables
COMMAND_OFF = 0
COMMAND_ON = 1
COMMAND_SET = 2
COMMAND_RESET = 3
STATUS_IDLE = 0
STATUS_ACTIVE = 1
ERROR_OK = 0
ERROR_COMMAND_FAILED = 1
ERROR_NON_RESPONSIBLE = -1


'''
This class is defined to create an Actuator Data in order to  interact with Raspberry Pi and the SenseHat
'''
class ActuatorData():
    
    
    
    
    #constrctor
    def __init__(self):
        self.updateTimeStamp()
        self.name = "Alert Message"
        self.hasError = False
        self.command = 0
        self.errCode = 0
        self.statusCode = 0
        self.stateData = None
        self.curValue = 0.0
    
    
    '''
    This function returns the value of command
    '''
    def getCommand(self):
        return self.command
    
    '''
    This function returns the name
    '''   
    def getName(self):
        return self.name
    
    '''
    This function returns the state of the data
    '''
    def getStateData(self):
        return self.stateData
    
    '''
    This function returns the  status code
    '''
    def getStatusCode(self):
        return self.statusCode
    
    '''
    This function is used to get error code if present
    '''
    def getErrorCode(self):
        return self.errCode
    
    '''
    This function returns the current value
    '''
    def getValue(self):
        return self.curValue;
    
    '''
    This function returns the error values
    '''
    def hasError(self):
        return self.hasError
    
    '''
    This function is used to set the command
    '''
    def setCommand(self, command):
        self.command = command
        
    '''
    This function is used to save the name
    '''
    def setName(self, name):
        self.name = name
        
    '''
    This function is used to save  the state data information
    '''
    def setStateData(self, stateData):
        self.stateData = stateData
        
    '''
    This function is used to store status code values
    '''
    def setStatusCode(self, statusCode):
        self.statusCode = statusCode
        
    '''
    this function  sets error code
    '''
    def setErrorCode(self, errCode):
        self.errCode = errCode
        if (self.errCode != 0):
            self.hasError = True
        else:
            self.hasError = False
            
    '''
    This class  is used as a setter to set values in Actuator Data. The val is the difference between current temp and nominal temp
    '''
    def setValue(self, curValue):
        self.curValue = curValue
        
    '''
    This class updates the object with updated values into the ActuatorData class
    '''
    def updateData(self, data):
        self.command = data.getCommand()
        self.statusCode = data.getStatusCode()
        self.errCode = data.getErrorCode()
        self.stateData = data.getStateData()
        self.curValue = data.getValue()
        
    def updateTimeStamp(self):
        self.timeStamp = str(datetime.now())
        
    '''
    Its a toString function to present the data in human readable format
    '''
    def __str__(self):
        customStr = \
        str(self.name + ':'  + \
        os.linesep + '\tTime: ' + self.timeStamp + \
        os.linesep + '\tCommand: ' + str(self.command) + \
        os.linesep + '\tStatus Code: ' + str(self.statusCode) + \
        os.linesep + '\tError Code: ' + str(self.errCode) + \
        os.linesep + '\tState Data: ' + str(self.stateData) + \
        os.linesep + '\tValue: ' + str(self.curValue))
        return customStr

