'''
Created on Jan 28, 2020

@author: HarshithaGS
'''

import os
from datetime import datetime

class SensorData(object):
    '''
    @param timeStame: current date and time
    @param curValue: Current temperature value
    @param avgValue: Average value of the temperature
    @param minValue: Minimum value of the temperature
    @param maxValue: maximum value of the temperature
    @param totalValue:  sum of the all values of temperature
    @param samples: count for the temperature reading 
    '''
   
    
    
    def __init__(self,name):
        '''
        constructor
        '''
        # store the current time and date
        self.timeStamp = str(datetime.now())
        self.name =name
        self.curValue = 0
        self.avgValue = 0
        self.minValue = 0
        self.maxValue = 0
        self.totValue = 0
        self.samples = 0
        
        
        
    def addValue(self, newVal):
        '''
        function to add the current temperature value and set
        the average, min, max temperature value accordingly
        '''
        self.samples += 1
        self.timeStamp = str(datetime.now())
        self.curValue = newVal
        self.totValue += newVal
        if (self.curValue < self.minValue or self.samples ==1):
            self.minValue = self.curValue
        if (self.curValue > self.maxValue or self.samples ==1):
            self.maxValue = self.curValue
        if (self.totValue != 0 and self.samples > 0):
            self.avgValue = self.totValue / self.samples
            
            
    def getAvgValue(self):
        '''
        returns the average temperature value
        '''
        return self.avgValue
    
    
    def getMaxValue(self):
        '''
        returns the maximum temperature value
        '''
        return self.maxValue
    
    
    def getMinValue(self):
        '''
        returns the minimum temperature value
        '''
        return self.minValue
    
    
    def getValue(self):
        '''
        returns the current temperature value
        '''
        return self.curValue
    
    def setName(self, name):
        '''
        set the name for the sensor
        '''
        self.name = name
    

    def __str__(self):
        customStr = \
        str( self.name + ':' + \
        os.linesep + '\tTime: ' + self.timeStamp + \
        os.linesep + '\tCurrent Value: ' + str(self.curValue) + \
        os.linesep + '\tAverage Value: ' + str(self.avgValue) + \
        os.linesep + '\tSample number: ' + str(self.samples) + \
        os.linesep + '\tMin: ' + str(self.minValue) + \
        os.linesep + '\tMax: ' + str(self.maxValue))
        return customStr
