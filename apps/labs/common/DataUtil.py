'''
Created on Feb 20, 2020

@author: HarshithaGS
'''
#imports
from labs.common import SensorData 
from labs.common import ActuatorData
import json

'''
This class handles conversions of SensorData and ActuatorData to / from JSON for use within the Redis database.
'''
class DataUtil(object):
    #Formats sensor and actuator data for display
    sensorData = None
    actuatorData = None
    jsonSdata = None
    jsonAdata = None
    
    def __init__(self):
        '''    
        Constructor
        @param  self.sensorData:instance of SensorData Class
        @param self.actuatorData :instance of Actuator Data Class 
        '''
        self.sensorData = SensorData.SensorData('Data')
        self.actuatorData = ActuatorData.ActuatorData()
        
    '''Converting Sensor Data to Json format'''    
    def toJsonFromSensorData(self,sensordata):
        #formats sensor data to json format
        self.jsonSdata = json.dumps(sensordata.__dict__,indent=2)
        self.writeSensorDatatoFile(self.jsonSdata)
        return self.jsonSdata
    
    '''Converting Actuator Data to Json format'''    
    def toJsonFromActuatorData(self,actuatordata):
        #formats actuator data to json format
        self.jsonAdata= json.dumps(actuatordata.__dict__,indent=2)
        self.writeActutatorDatatoFile(self.jsonAdata)
        return self.jsonAdata
    
    '''Converting Json format of Actuator Data to Actuator Data format'''    
    def toActuatorDataFromJson(self,jsonData):
        adDict = json.loads(jsonData)
        ad = self.actuatorData
        ad.name = adDict['name']
        ad.timeStamp = adDict['timeStamp']
        ad.command = adDict['command']
        ad.stateData = adDict['stateData']
        ad.curValue = adDict['curValue']  
        return ad

    '''Converting Json format of Sensor Data to Sensor Data format'''        
    def toSensorDataFromJson(self,jsonData):
        sdDict = json.loads(jsonData)
        sd=self.sensorData
        sd.name = sdDict['name']
        sd.timeStamp = sdDict['timeStamp']
        sd.avgValue = sdDict['avgValue']
        sd.minValue = sdDict['minValue']
        sd.maxValue = sdDict['maxValue']
        sd.curValue = sdDict['curValue']
        return sd
    
    '''Writing the Sensor Data to a file foramt'''
    def writeSensorDatatoFile(self,Sensor):
        outputSd = open('sensordata.txt','a+')
        outputSd.write(Sensor)
        
    '''Writing the Actuator Data to a file foramt'''
    def writeActutatorDatatoFile(self,Actuator):
        outputAd = open('actuatordata.txt','a+')
        outputAd.write(Actuator) 