'''
Created on Feb 20, 2020

@author: HarshithaGS
'''
#imports
import redis
from labs.common.DataUtil import DataUtil
from labs.common.ActuatorDataListener import ActuatorDataListener
from labs.common import ConfigUtil

'''
This class handles the following:
1. Uses DataUtil to handle conversions of SensorData and ActuatorData to / from JSON for use within the Redis database.
2. Write SensorData to Redis
3. Reads From Redis the Actuator Data
'''
class PersistenceUtil(object):

    config = ConfigUtil.ConfigUtil()


    def __init__(self):
        '''
        Constructor
        @param redis_host: Host address of redis read from Configuartions.props file
        @param redis_port:  Port address of redis read from Configuartions.props file
        @param redis_password:AUth Key of redis read from Configuartions.props file 
        @param self.r_server:launches redis 
        @param self.dataU: instance of Data Util
        @param  self.actListener: instance of   ActuatorDataListener
        '''
        redis_host = (self.config.getProperty(self.config.configConst.REDIS, self.config.configConst.REDIS_HOST))
        redis_port = int ((self.config.getProperty(self.config.configConst.REDIS, self.config.configConst.REDIS_PORT)))
        redis_password = (self.config.getProperty(self.config.configConst.REDIS, self.config.configConst.REDIS_AUTH))
        
        self.r_server = redis.Redis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
        self.dataU=DataUtil()
        self.actListner=ActuatorDataListener();
        self.flag =0
        self.i=1
        self.j=1
        self.h =2
        self.i2c=3
        
    '''Function to Write Sensor Data to Redis '''          
    def writeToRedis(self,SensorData  , text):
        if (text == "Humidity"):
            json_values=self.dataU.toJsonFromSensorData(SensorData )
            self.dataU.writeSensorDatatoFile(json_values)
            self.flag=0
            key="sample"+str(self.h)
            self.r_server.set(key, json_values)
            print("published")
            self.flag=1  
            self.h+=1
            return ("Data is set to Redis")
        elif(text=="I2C"):
            json_values=self.dataU.toJsonFromSensorData(SensorData )
            self.dataU.writeSensorDatatoFile(json_values)
            self.flag=0
            key="sample"+str(self.i2c)
            self.r_server.set(key, json_values)
            print("published")
            self.flag=1
            self.i2c+=1
            return ("Data is set to Redis")
        else:
            json_values=self.dataU.toJsonFromSensorData(SensorData )
            self.dataU.writeSensorDatatoFile(json_values)
            self.flag=0
            key="sample"+str(self.i)
            self.r_server.set(key, json_values)
            print("published")
            self.flag=1           
            self.i+=1
            return ("Data is set to Redis")

        
    '''Function to read Actuator Data from Redis'''    
    def ReadfromRedis(self ):
        
        self.flag=0
        key="actuate"+str(self.j)
        data=self.r_server.get(key)
        self.flag=1
        self.actListner.onMessage()
        self.j+=1
        self.dataU.writeActutatorDatatoFile(data)
        return data
    
    '''This function returns a flag from Java Gateway  to say that all the ACtuator Data is written into redis for device '''
    def ReadfromRedisFlag(self):
        key = "done"
        marker=self.r_server.get(key)
        print(marker)
        return marker
        