'''
Created on Jan 28, 2020

@author: HarshithaGS
'''
class ConfigConst(object):
    
    ''' 
    Configuration Constant class  for representing the sections and Key values from configuration values
    '''
    SECTION_SEPARATOR='.'
    DEFAULT_CONFIG_FILE_NAME = '../../../config/ConnectedDevicesConfig.props'

    
    CLOUD = 'cloud'
    SMTP = 'smtp'
    GATEWAY_DEVICE = 'gateway'
    CONSTRAINED_DEVICE = 'device'
    REDIS = 'redis'
    MQTT = 'mqtt'
    COAP = 'coap';


    SMTP_CLOUD_SECTION = SMTP + SECTION_SEPARATOR + CLOUD
    COAP_CLOUD_SECTION = COAP + SECTION_SEPARATOR + CLOUD;
    COAP_DEVICE_SECTION = COAP + SECTION_SEPARATOR + CONSTRAINED_DEVICE;



    FROM_ADDRESS_KEY = 'fromAddr'
    TO_ADDRESS_KEY = 'toAddr'
    HOST_KEY = 'host'
    PORT_KEY = 'port'
    COAP_HOST = 'coapHost';
    COAP_PORT = 'coapPort';

    USER_AUTH_TOKEN_KEY = 'authToken'
    ENABLE_AUTH_KEY = 'enableAuth'
    ENABLE_CRYPT_KEY = 'enableCrypt'
    ENABLE_EMULATOR_KEY = 'enableEmulator'
    ENABLE_LOGGING_KEY = 'enableLogging'
    POLL_CYCLES_KEY = 'pollCycleSecs'
    KEEP_ALIVE_KEY = 'keepAlive'
    NOMINALVAL = 'nominalVal'
    
    REDIS_HOST = 'redis_host'
    REDIS_PORT = 'redis_port'
    REDIS_AUTH  = 'redis_auth'
    
    MQTT_HOST = 'mqtt.eclipse.org'
    MQTT_PORT = '1883'
    MQTT_QoS = '2'
    


    def __init__(self):
        '''
        Constructor
        '''
    

   