'''
Created on Jan 28, 2020

@author: HarshithaGS
'''
import configparser
import os
from labs.common import ConfigConst

class ConfigUtil(object):
    '''
    This Class helps in loading , reading and getting properties or values from the default configuration file
    @param configConst: instantiation for ConfigConst class 
    '''
    configData = None 
    isLoaded = False  
    configConst = None 
    

    def __init__(self):
        '''
        Constructor
        '''
        self.configConst = ConfigConst.ConfigConst()
        self.configData = configparser.ConfigParser()
        self.configFilePath = self.configConst.DEFAULT_CONFIG_FILE_NAME
        
    def loadConfig(self):
        '''
        This method loads the configuration file and sets the isLoaded variable as true
        First the path is checked to see whether the configuration file exists or not. If yes then the file is read
        '''
        if(os.path.exists(self.configFilePath)): 
            self.configData.read(self.configFilePath) 
            self.isLoaded = True
          
           
    def getConfigData(self,forceReload=False):
        '''
        this method returns the configuration data  if it is not loaded or there is a forceReload function set True 
        @return: configuration data
        '''
        if(self.isLoaded==False or forceReload): 
            self.loadConfig()  
        return self.configData
    
    
    def getConfigFile(self):
        '''
        this method returns the configuration file path
        '''
        return self.configFilePath
    
    
    def getProperty(self,section,key):
        '''
        this method returns  key from a section of  the configuration file
        '''
        return self.getConfigData(forceReload=False).get(section, key)
    
    
    def isConfiguredDataLoaded(self):
        '''
        this method checks whether the configuartion file is loaded . Then it  returns the boolean variable isLoaded 
        '''
        return self.isLoaded
    
            
    
        
            