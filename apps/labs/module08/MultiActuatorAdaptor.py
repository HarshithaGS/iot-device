'''
Created on Mar 24, 2020

@author: HarshithaGS
'''

from labs.common import ActuatorData
from labs.common import PersistenceUtil
from labs.common import DataUtil
from labs.module04 import SenseHatLedActivator
from sense_hat import SenseHat

class MultiActuatorAdaptor():
    '''
    classdocs
    '''  
    actuatorData = None
    senseHat = None
    senselib=None

    def __init__(self):
        '''
        Constructor
        '''
        self.actuatorData = ActuatorData.ActuatorData()
        self.senseHat=SenseHatLedActivator.SenseHatLedActivator()
        self.senselib=SenseHat()
        self.red=[255,0,0]
        self.green=[0,255,0]
        self.blue=[0,0,255]    
        self.persist=PersistenceUtil.PersistenceUtil();
        self.util=DataUtil.DataUtil();
        
    def processMessage(self,message):
        #compare the actuator data received and previously stored actuator instance of this class
        
            #if the actuator value is greater than zero i.e temperature is above nominal show decrease message
            if(message=="21"):
                msg="Inc"
                tcolor=self.red  
            else:
                msg="Dec"
                tcolor=self.blue
                

            #create new instance
            senseHat = SenseHatLedActivator.SenseHatLedActivator()
            senseHat.setDisplayMessage(msg)
            print("Actuator-", msg)
            #set the enableled flag to true
            senseHat.enableLed=True
            try:
                #display message method invoked from sensehat library
                senseHat.displayMessage();
                self.senselib.show_message(msg ,text_colour=tcolor);
            except:
                print("____");
            finally:
                senseHat.enableLed=False;               
                #return positive boolean if triggering actuator is successful
                return True
