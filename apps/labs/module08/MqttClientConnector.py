'''
Created on Mar 23, 2020

@author: HarshithaGS
'''

import paho.mqtt.client as mqtt
from labs.module08.MultiActuatorAdaptor import MultiActuatorAdaptor
import time # importing time to perform time operations

class MqttClientConnector():
    host = "mqtt.eclipse.org"
    json_data = "Hello"
    Connected = False
    Multiact=MultiActuatorAdaptor()
    
    '''
    Callback function: This function will execute once the client connects
    with MQTT Broker
    '''
   
        #client.subscribe(self.topic)
    def on_connect(self,client, userdata, flags, rc):
 
        if rc == 0:
 
            print("Connected to broker")
 
            global Connected                #Use global variable
            Connected = True                #Signal connection 
 
        else:
 
            print("Connection failed")
 

    
    '''
    Constructor
    @param topic:topic of the message published or subscribed 
    ''' 
    def __init__(self,topic="ActuatorData_H"):
        self.topic = topic;
        self.multiact=MultiActuatorAdaptor()
        
    '''
    Function is used to publish the message
    @param topic: Topic of the message
    @param message: Message to be sent
    @param host: address of MQTT broker  
    @param qos:qos value  
    '''
    def publish(self,topic,message,host ,qos):
        client = mqtt.Client();
        client.connect(host,1883)
        client.publish(topic,message ,qos)
        print("Published")
        client.loop_start()
        time.sleep(50)
        client.loop_stop()
        client.disconnect()
        return str(message)
    '''
    Function is used to subscribe to a topic
    @param host: Address of MQTT broker 
    '''
    def subscribe(self,host):
        
        client = mqtt.Client("harshi")
        client.on_message = self.on_message
        client.connect(host)
        client.loop_start() 
        client.subscribe("ActuatorData_H",1)
        time.sleep(50)
        client.disconnect()
        client.loop_stop()
    
    '''
    Function is used to store the data received from MQTT Broker
    @return: Data received from MQTT Broker
    '''
    def on_message(self, client, userdata, message):
        print("hi")
        print ("Message received: "  + str(message.payload.decode("utf-8")))
        self.multiact.processMessage(str(message.payload.decode("utf-8")))
    
   
        
        
        
