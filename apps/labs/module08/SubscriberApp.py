'''
Created on Mar 23, 2020

@author: HarshithaGS
'''



from labs.common import SensorData
from labs.common.SensorDataListener import SensorDataListener
from labs.common import ConfigUtil
from sense_hat import SenseHat
from labs.module08 import MqttClientConnector
from labs.common import DataUtil
from labs.common import ConfigConst

class SubscriberApp():
    '''
    call run method to publish updated sensor data to mqtt broker
    '''
    config = ConfigUtil.ConfigUtil()
    def __init__(self):
        '''
        Constructor
        '''
        #Run the threads simultaneously
       
        self.senseHat=SenseHat()
        self.sensorData=SensorData.SensorData("data")
        #the duration after which new humidity value is generated
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))
        self.senseListner=SensorDataListener();
        #instance if mqttclient connector class
        self.mqttconnect=MqttClientConnector.MqttClientConnector()    
        self.data = DataUtil.DataUtil()
        self.host = ConfigConst.ConfigConst.MQTT_HOST

        
   
    def run(self):
        '''
        @param curTemp:take temperature value from sensehat
        '''
 
        while True: 
            self.mqttconnect.subscribe("mqtt.eclipse.org")
        
                