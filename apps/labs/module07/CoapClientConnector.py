'''
Created on Mar 13, 2020

@author: HarshithaGS
'''

from coapthon.client.helperclient import HelperClient 

class CoapClientConnector():
    '''
    This class is a helper class which helps to  connect to the CoAP server
    
    Attributes:
        @param host - IP address of the server
        @param port - Port number to connect to
        @param path  - Resource URI
    '''
    
    def __init__(self, host, port, path):
        self.host = host
        self.port = port
        self.path = path
        self.client = HelperClient(server=(host, port))
        
    
    def ping(self):
        '''
        THis is a Wrapper method to ping the server
        '''
            
        self.client.send_empty("")
        
    def get(self):
        '''
        This is a Wrapper method for the GET action
        '''
        
        response = self.client.get(self.path)
        print(response)
    
    def post(self,jsonData):
        '''
        This is a Wrapper method for the POST action
        '''
        
        response  = self.client.post(self.path, jsonData)
        print(response)
    
    def put(self, jsonData):
        '''
        Wrapper method for the PUT action
        
        Parameters:
            jsonData (str) - Request payload in JSON format
        '''
        
        response = self.client.put(self.path, jsonData)
        print(jsonData)
        print(response)
        
    def delete(self):
        '''
        This is a Wrapper method for the DELETE action
        '''
            
        response = self.client.delete(self.path)
        print(response)
        
    def stop(self):
        '''
        This method stops the client thread
        '''
        
        self.client.stop()    