'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.module06 import TempSensorAdaptorTask  ,  HumiditySensorAdaptorTask  
from time import sleep
from labs.common import ConfigUtil
from threading import Thread

'''
This class instantiates the MultiSensorAdaptor Class which calls the 2 threading classes 
'''
class MultiSensorAdaptor:
    #constructor
    def __init__ (self):
        
        '''
        @param self.temptask   : instance of TempSensorAdaptorTask
        @param self.humidtask  : instance of HumiditySensorAdaptorTask
        @param self.adaptortask: instance of the definition of adaptortask
        '''
        
        self.temptask     = TempSensorAdaptorTask.TempSensorAdaptorTask(0.5,'Temperature Reading is')
        self.humidtask    = HumiditySensorAdaptorTask.HumiditySensorAdaptorTask(0.5,'Humidity Reading is')
        
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))

        
        self.adaptorstart(self.temptask , self.humidtask)
        
    '''
    This method is to start the threads present in the respective Task's class   
    '''
    def adaptorstart(self, temptask ,humidtask):
        Thread(temptask.start())
        sleep(6)
        print("---------********----------")
        Thread(humidtask.start())
        sleep(6)
        print("---------********----------") 



    
        
