'''
Created on Feb 13, 2020

@author: HarshithaGS
'''

from threading import Thread # importing thread 
from time import sleep
from labs.common import SensorData # importing SensorData class to use the attributes of sensor
from sense_hat import SenseHat #importing the SenseHat Package
from labs.common import PersistenceUtil
from labs.common.SensorDataListener import SensorDataListener 
from labs.module06 import MqttClientConnector
from labs.common import DataUtil
from labs.common import ConfigConst
 
'''
This class  uses  SenseHat Lib/API from the sense_hat module in order to read the humidity from the SenseHAT
'''        
class HumiditySensorAdaptorTask(Thread):
    #constructor
    def __init__(self,alertdiff , name):
        '''
        @param enableEmulator: boolean value to start the emulator
        @param sensorData: instantiation of sensorData class
        @param connector: instantiation of smtpClientConnector class
        @param timeInterval: time difference post which new temperature  value is generated
        @param alertDiff: the threshold value for sending alert message and notify user
        @param current_Temp: current value for  temperature  
        @param self.data : instance of DataUtil
        @param self.host : instnace of MQTt host
        @param self.mqttconnect : instance of MqttClientConnector  
        '''
        super(HumiditySensorAdaptorTask, self).__init__()# instance of subclass of Thread class
        Thread.__init__(self) # Overriding the __init__(self [,args]) method to add additional arguments.
        
        self.enableEmulator = True
        self.sensorData = SensorData.SensorData(name)
        self.alertDiff = alertdiff
        self.sensehatLib = SenseHat()
        self.persistance=PersistenceUtil.PersistenceUtil()
        self.senseListner=SensorDataListener();
        self.mqttconnect=MqttClientConnector.MqttClientConnector()
        self.data = DataUtil.DataUtil()        
        self.host = ConfigConst.ConfigConst.MQTT_HOST

    '''
    This run method will generate a random float variable(Current Value) and sends thisdata to the DeviceDataManager to compare certain conditions in order to send the email notification to user
    '''
               
    def run(self):   
        count=0
        while True:
            if(count<=3):
                if self.enableEmulator:
                    self.current_Humidity = self.sensehatLib.get_humidity()
                    self.sensorData.addValue(self.current_Humidity)
                    #printing  the sensor data 
                    print('-----------------------------------------------')
                    print(str(self.sensorData))
                    json_humiditydata = self.data.toJsonFromSensorData(self.sensorData)
                    #publishing data via Mqtt 
                    self.mqttconnect.publish("SensorData",json_humiditydata,self.host,2)      
                    
                    count+=1
            else:
                self.senseListner.onMessage()
                break;
            sleep(5);           
        return True   
        