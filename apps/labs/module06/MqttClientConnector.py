'''
Created on Feb 28, 2020

@author: HarshithaGS
'''
import paho.mqtt.client as mqtt
import time 
from labs.common import ConfigConst

class MqttClientConnector():
    
    host = ConfigConst.ConfigConst.MQTT_HOST
    
    '''
    Constructor
    @param topic:topic of the message published or subscribed 
    ''' 
    def __init__(self,topic=None):
        self.topic = topic;
    
    '''
    Callback function: This function will execute once the client connects
    with MQTT Broker
    '''
    def on_connect(self,client,flags,rc):
        print("Connected with Client: "+str(rc))
        client.subscribe(self.topic)
    
    '''
    Callback function: This function will execute once the client receives message
    from MQTT Broker
    ''' 
    def on_message(self,client,
                   msg):
        global json_data
        json_data = str(msg.payload.decode("utf-8"))
        client.loop_stop()
       
    '''
    Function is used to publish the message
    @param topic: Topic of the message
    @param message: Message to be sent
    @param host: address of MQTT broker   
    '''
    def publish(self,topic,message,host ,qos):
        client = mqtt.Client();
        client.connect(host,1883)
        client.loop_start()
        client.publish(topic,message ,qos)
        time.sleep(60)
        client.loop_stop()
        client.disconnect()
        return str(message)

    '''
    Function is used to store the data received from MQTT Broker
    @return: Data received from MQTT Broker
    '''
    def message(self):
        global json_data
        return json_data
