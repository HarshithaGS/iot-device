'''
Created on Jan 28, 2020

@author: HarshithaGS
'''
#Standard imports
from labs.common import ConfigUtil
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging

class SmtpClientConnector(object):
    '''
    Publishes email using SMTP protocol
    '''

    def __init__(self):
        '''
        Constructor
        @param self.config : instantiation of ConfigUtil class
        @param self.config.loadConfig(): load the default configuartion file 
        '''
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        
              
    def publishMessage(self,topic,data):
        '''
        this method allows  to send email with email subject and message text as the input parameters
        @param host , port , fromAddr , toAddr , authToken are the variables needed to email the user directly obtained from the configuration file 
        '''

        host = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.HOST_KEY) 
        port = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.PORT_KEY) 
        fromAddr = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.FROM_ADDRESS_KEY) 
        toAddr = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.TO_ADDRESS_KEY) 
        authToken = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.USER_AUTH_TOKEN_KEY) 
        
        '''
        @param msg:container variable for sending email message
        @param msg['from']: sender's email address
        @param msg['to']: receiver's email address
        @param msg['subject']:Email's subject text    
        @param smtpServer:stmp instance to send the email alert 
        '''

        msg = MIMEMultipart() 
        msg['from'] = fromAddr #senders address
        msg['to'] = toAddr #receivers address
        msg['subject'] = topic 
        msgBody = str(data) 
        msg.attach(MIMEText(msgBody)) 
        msgText = msg.as_string()
        
        
        try:  
            smtpServer = smtplib.SMTP_SSL(host, port);#Connecting SMTP server at given port number.
            smtpServer.ehlo();#method calling SMTP extended hello
            smtpServer.login(fromAddr, authToken); # login to the mail with the username and password
            smtpServer.sendmail(fromAddr, toAddr, msgText);
            smtpServer.close();
        except:
            raise Exception #exception raised if mail is not sent succesfully
            logging.info("Error in Sending Mail");
        
        