'''
Created on Jan 28, 2020

@author: HarshithaGS
'''
#Standard Imports
import threading
from labs.common import SensorData
from labs.module02 import SmtpClientConnector
from labs.common import ConfigUtil
from time import sleep
import random

class TempSensorEmulatorTask(threading.Thread):
    
    #instantiation of configutil
    config = ConfigUtil.ConfigUtil()
    
    #Constructor
    def __init__(self,alertdiff):
        '''
        @param enableEmulator: boolean value to start the emulator
        @param sensorData: instantiation of sensorData class
        @param connector: instantiation of smtpClientConnector class
        @param timeInterval: time difference post which new temperature  value is generated
        @param alertDiff: the threshold value for sending alert message and notify user
        @param lowValue: lowest value for temperature
        @param highValue: highest value for  temperature
        @param current_Temp: current value for  temperature      
        '''
        threading.Thread.__init__(self)
        self.enableEmulator = False
        self.sensorData = SensorData.SensorData("data")
        self.connector =  SmtpClientConnector.SmtpClientConnector()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))
        self.alertDiff = alertdiff
        self.lowValue = 0
        self.highValue = 30
        self.current_Temp = 0
        
            
    def setEmulator(self,boolean):
        '''
        This method will set the enableEmulator to boolean value to start the emulator
        '''
        self.enableEmulator = boolean
        
     
    def getSensorData (self):
        #The self.curTemp is the variable holds the randomly generated temperature value between 0 and 30
        self.current_Temp = random.uniform(float(self.lowValue),float(self.highValue))
        #The data is added to SensorData
        self.sensorData.addValue(self.current_Temp)
        print('-----------------------------------------------')
        #printing  the sensor data 
        print(str(self.sensorData))
        sensor_data = self.sensorData
        print(sensor_data)
        return sensor_data  
            
    def run(self):
        '''
        This method runs when the thread is started
        '''
        threading.Thread.run(self)
        while True:
            if(self.enableEmulator):
                #The self.curTemp is the variable holds the randomly generated temperature value between 0 and 30
                self.current_Temp = random.uniform(float(self.lowValue),float(self.highValue))
                #The data is added to SensorData
                self.sensorData.addValue(self.current_Temp)
                print('-----------------------------------------------')
                #print the sensor data 
                print(str(self.sensorData))
                # A triggering  alert is sent to the email via  SMTP message when this  condition is true
                if(abs(self.current_Temp - self.sensorData.getAvgValue())>= self.alertDiff):
                    print('\n The Current Temperature exceeds the average by ' + str(self.alertDiff) + ' Hence triggering the alert message')
                    self.connector.publishMessage('The Temperature Alert message is : ', self.sensorData)
                sleep(self.timeInterval)
                    
            
            
        
        