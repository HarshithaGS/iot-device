'''
Created on Jan 28, 2020

@author: HarshithaGS
'''
import time

from labs.module02 import TempSensorEmulatorTask

''' 
@param TempSET: instance of TempSensorEmulatorTask created with threshold value of 10
'''
class TempEmulatorAdaptor :
    
    def __init__(self):
        TempSET = TempSensorEmulatorTask.TempSensorEmulatorTask(10)
        # enables the emulator
        TempSET.setEmulator(True)
        #starts the thread
        TempSET.start()
        time.sleep(3)

