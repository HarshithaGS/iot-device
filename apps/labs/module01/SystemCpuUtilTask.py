'''
Created on Jan 24, 2020

@author: HarshithaGS
'''

'''
This class will get the CPU Usage Performance Data
'''
import psutil
class SystemCpuUtilTask:
    def getDataFromSensor(self):
        return psutil.cpu_percent(interval=1)

