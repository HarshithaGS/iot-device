'''
Created on Jan 24, 2020

@author: HarshithaGS
'''

'''
This class will get the Memory Usage Performance Data
'''
import psutil
class SystemMemUtilTask:
    def getDataFromSensor(self):
        return psutil.virtual_memory().percent