from labs.module01.SystemMemUtilTask import SystemMemUtilTask
from labs.module01.SystemCpuUtilTask import SystemCpuUtilTask
from time import sleep, asctime
import logging
from threading import Thread

'''
This class Calls the CpuUtil and MemUtil  classes t obtain the cpu and mem usage values and log them 
'''

class SystemPerformanceAdaptor():
    # Constructor
    def __init__(self):
        
        Thread.__init__(self)
        self.enableAdaptor = True
     

    '''
    This run method collects the data and prints it in the format given below
    '''          
    def run(self):
        if self.enableAdaptor:
            cpuUtil = SystemCpuUtilTask.getDataFromSensor(self)
            memUtil = SystemMemUtilTask.getDataFromSensor(self)
            performanceUsageData = 'CPU Utilization=' + str(cpuUtil) + '; ' + 'Memory Utilization=' + str(memUtil)
            logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s' ,level =logging.DEBUG)
            logging.info(performanceUsageData)
