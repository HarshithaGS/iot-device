from time import sleep
from labs.module01.SystemPerformanceAdaptor import SystemPerformanceAdaptor


'''
This is the Main Class App whch will initialise the SystemPerformanceAdapter class and run at timed intervals
'''
systemPerfAdaptor = None
systemPerfAdaptor = SystemPerformanceAdaptor()
systemPerfAdaptor.daemon = True

while (True):
    systemPerfAdaptor.run()
    sleep(5)


