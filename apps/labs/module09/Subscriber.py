'''
Created on Apr 13, 2020

@author: HarshithaGS
'''
from labs.module09  import MqttClientConnector

class Subscriber():
    '''
    call run method to subscribe to updated data sent from java api
    '''
    def __init__(self):
        '''
        Constructor
        '''
        #instance of mqttclient connector class
        self.mqttconnect=MqttClientConnector.MqttClientConnector()    
   
    def run(self):
        '''
        @param mqttconnect:calling subscribe method of mqtt client connector class
        '''
        while True: 
            self.mqttconnect.subscribe("mqtt.eclipse.org")
            return "true"
