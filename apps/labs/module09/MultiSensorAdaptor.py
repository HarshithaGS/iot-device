'''
Created on Apr 13, 2020

@author: HarshithaGS
'''
from labs.module09  import TempSensorAdaptorTask
from time import sleep

class MultiSensorAdaptor:
    def __init__(self):
        #boolean to enable the emulator
        self.enableEmulator = True
        self.temptask=TempSensorAdaptorTask.TempSensorAdaptorTask()
              
    #Main Run method calling for all TempSensorAdaptorTask instance
    def AdaptorRunMain(self):
        '''
        Run mutiple threaded class simultaneously
        @param temptask: Run temperature task thread
        '''
        
        self.temptask.setEmulator(True)
        #call temp sensor task
        self.temptask.start();
        sleep(6)
        print('_._._._._._._._._._._._._._._._._._._._._._')