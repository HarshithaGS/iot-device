'''
Created on Apr 13, 2020

@author: HarshithaGS
'''
from time import sleep
import threading
from labs.common import ConfigUtil
from sense_hat import SenseHat
from labs.module09 import AWSPub

class TempSensorAdaptorTask(threading.Thread):
    '''
    call run method to publish updated sensor data to coap broker
    '''
    config = ConfigUtil.ConfigUtil()
    def __init__(self):
        '''
        Constructor
        '''
        #Run the threads simultaneously
        super(TempSensorAdaptorTask, self).__init__()
        threading.Thread.__init__(self)
        self.senseHat=SenseHat()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))

         
    def setEmulator(self,boolean):
        #set the boolean value to start the emulator
        self.enableEmulator = boolean;
        
    def run(self):
        '''
        @param curTemp:take temperature value from sensehat
        '''
        threading.Thread.run(self)
        count=1
        while True:
            if(count<=10):
                #run for 10 sequences
                if(self.enableEmulator):
                    #get temperature from SenseHat
                    self.curTemp = self.senseHat.get_temperature() 
                    #calling  Aws publisher instance to pass the values to publisher method
                    AWSPub.publisher(self.curTemp,count)
                    count+=1
            else:
                break
            sleep(5);  