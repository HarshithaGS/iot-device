'''
Created on Apr 13, 2020

@author: HarshithaGS
'''

from labs.module04 import SenseHatLedActivator
from sense_hat import SenseHat

class MultiActuatorAdaptor():

    actuatorData = None
    senseHat = None
    senselib=None

    def __init__(self):
        '''
        Constructor to initialize all instances
        '''
        self.senseHat=SenseHatLedActivator.SenseHatLedActivator()
        self.senselib=SenseHat()
        
    # Show Actuator output 
    def processMessage(self,message):
            msg="Actuator set"
            try:
                self.senselib.show_message(msg);
            except:
                print("____");
            finally:
                self.senseHat.enableLed=False;

