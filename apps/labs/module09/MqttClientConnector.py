'''
Created on Apr 13, 2020

@author: HarshithaGS
'''

import paho.mqtt.client as mqtt
from labs.module09.MultiActuatorAdaptor import MultiActuatorAdaptor
import time 

class MqttClientConnector():
    host = "mqtt.eclipse.org"
    json_data = "Hello"
    Connected = False
    Multiact=MultiActuatorAdaptor()
    
    '''
    Callback function: This function will execute once the client connects
    with MQTT Broker
    '''

    def on_connect(self,client, userdata, flags, rc):
        # check if connected successfully
        if rc == 0:
            print("Connected to broker")
            global Connected                #Use global variable
            Connected = True                #Signal connection 
        else:
            print("Connection failed")
 
    def on_message(self, client, userdata, message):
        #check the message received and convert the payload for display
        print (" New Message received: "  + str(message.payload.decode("utf-8")))
        #pass the message to actuator class
        self.multiact.processMessage(str(message.payload.decode("utf-8")))
    
    '''
    Constructor
    @param topic:topic of the message published or subscribed 
    @param multiact:instance of multiactuatoradaptor class
    ''' 
    def __init__(self,topic="aws"):
        self.topic = topic;
        self.multiact=MultiActuatorAdaptor()
        
    '''
    Function is used to publish the message
    @param topic: Topic of the message
    @param message: Message to be sent
    @param host: address of MQTT broker  
    @param qos:qos value  
    '''
    def publish(self,topic,message,host ,qos):
        client = mqtt.Client();
        client.connect(host,1883)
        client.publish(topic,message ,qos)
        print("Published")
        client.loop_start()
        time.sleep(50)
        client.loop_stop()
        client.disconnect()
        return str(message)
    '''
    Function is used to subscribe to a topic
    @param host: Address of MQTT broker 
    '''
    def subscribe(self,host):        
        client = mqtt.Client("harshitha")
        client.on_message = self.on_message
        client.connect(host)
        client.loop_start() 
        client.subscribe("aws",1) 
        time.sleep(50)
        client.disconnect()
        client.loop_stop()
    


   
        
        
        
