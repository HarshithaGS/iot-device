'''
Created on Apr 13, 2020

@author: HarshithaGS
'''

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json

# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")

# Read in command-line parameters
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", action="store", dest="port", type=int, help="8883")
parser.add_argument("-id", "--clientId", action="store", dest="clientId", default="basicPubSub",
                    help="Targeted client id")
parser.add_argument("-t", "--topic", action="store", dest="topic", default="sdk/test/Python", help="Targeted topic")

args = parser.parse_args()
host = "aqg86h4a4i5jr-ats.iot.us-east-2.amazonaws.com"
rootCAPath = "C:\\Users\\HarshithaGS\\Desktop\\AWSExtraHW\\AmazonRootCA1.pem"
certificatePath = "C:\\Users\\HarshithaGS\\Desktop\\AWSExtraHW\\ee30e7c98b-certificate.pem.crt"
privateKeyPath = "C:\\Users\\HarshithaGS\\Desktop\\AWSExtraHW\\ee30e7c98b-private.pem.key"
port = args.port
clientId = args.clientId
topic = args.topic

# Port defaults
if not args.port:  # When no port override for non-WebSocket, default to 8883
    port = 8883

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureEndpoint(host, port)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()

# Publish to the same topic in a loop
def publisher(tempreading,loopCount): 
    message = {}  
    message['message'] = tempreading
    message['sequence'] = loopCount
    messageJson = json.dumps(message)
    myAWSIoTMQTTClient.publish(topic, messageJson, 1)
    print("msg sent: sequence no: " + "%d" %loopCount +" Temperature " + "%.2f" % tempreading )
    print('Published topic %s: %s\n' % (topic, messageJson))    
    time.sleep(1)      