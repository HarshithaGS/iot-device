'''
Created on Feb 13, 2020

@author: HarshithaGS
'''
#imports
import smbus2 as SMBus # import SMBUS
from threading import Thread # importing thread 
from time import sleep
from labs.common import  SensorData
import sys

sys.path.append('/home/pi/workspace/iot-device/apps')


enableEmulator = False
i2cBus = SMBus.SMBus(1)  # Use I2C bus No.1 on Raspberry Pi3 +
enableControl = 0x2D
enableMeasure = 0x08
accelAddr = 0x1C         # address for IMU (accelerometer)
magAddr = 0x6A           # address for IMU (magnetometer)
pressAddr = 0x5C         # address for pressure sensor
humidAddr = 0x5F         # address for humidity sensor
begAddr = 0x28
totBytes = 6
H_OUT_L = 0x28
H_OUT_M = 0x29
H0_rH_x2 = 0x30
H1_rH_x2 = 0x31
H0_TO_OUT_L = 0x36 # 8 bit caliberated register 
H0_TO_OUT_M = 0x37 # 8 bit caliberated register
H1_TO_OUT_L = 0x3A # 8 bit caliberated register 
H1_TO_OUT_M = 0x3B # 8 bit caliberated register 
H_T_OUT = 0 # reading humidity values in raw count

class I2CSensorAdaptorTask(Thread):
     
    #constructor
    def __init__(self ,alertdiff, name):
        
        self.enableEmulator = True
             
        super(I2CSensorAdaptorTask, self).__init__()# instance of subclass of Thread class
        Thread.__init__(self) # Overriding the __init__(self [,args]) method to add additional arguments.

        self.name=name
        self.initI2CBus()
        self.sensorData =SensorData.SensorData(name)
        self.alertDiff =alertdiff
        
    '''Initializing I2C bus and enabling I2C addresses'''
    def initI2CBus(self):
        i2cBus.write_byte_data(accelAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(magAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(pressAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(humidAddr, enableControl, enableMeasure)
   
    '''This def calculates the humidity. Since H0_TO_OUT and H1_TO_OUT are values obtained by combining putput from two 2 unsigned 8 bit registers. Hence the signed bit needs to be handled. 
    Also, the format to read the data from i2c bus is data = i2cBus.read_i2c_block_data({sensor address}, {starting read address}, {number of bytes}.'''
    def humidity(self):
        self.H0_rh = (i2cBus.read_byte_data(humidAddr, H0_rH_x2))/2.0
        self.H1_rh = (i2cBus.read_byte_data(humidAddr, H1_rH_x2))/2.0
        
        #Reading value of H0_TO_OUT
        self.H0_TO_OUT_L= (i2cBus.read_byte_data(humidAddr, H0_TO_OUT_L))
        self.H0_TO_OUT_H= (i2cBus.read_byte_data(humidAddr, H0_TO_OUT_M))
        self.H0_TO_OUT= self.H0_TO_OUT_L + (self.H0_TO_OUT_H<<8)
        if self.H0_TO_OUT & (1 << 16 -1):
            self.H0_TO_OUT-=(1<<16)
        print("H0_TO_OUT:",self.H0_TO_OUT)
        
        #Reading value of H1_TO_OUT
        self.H1_TO_OUT_L= (i2cBus.read_byte_data(humidAddr, H1_TO_OUT_L))
        self.H1_TO_OUT_H= (i2cBus.read_byte_data(humidAddr, H1_TO_OUT_M))
        self.H1_TO_OUT= self.H1_TO_OUT_L + (self.H1_TO_OUT_H<<8)
        if self.H1_TO_OUT & (1 << 16 -1):
            self.H1_TO_OUT-=(1<<16)
        print("H1_TO_OUT:",self.H1_TO_OUT)
        
        #Reading Value of H_OUT
        self.H_T_OUT_L = (i2cBus.read_byte_data(humidAddr, H_OUT_L))
        self.H_T_OUT_M = (i2cBus.read_byte_data(humidAddr, H_OUT_M))
        self.H_T_OUT= self.H_T_OUT_L + (self.H_T_OUT_M<<8)
        print("H_TO_OUT:",self.H_T_OUT)
        
        #Calculating the value of humidity in float 
        hrH = (((self.H1_rh - self.H0_rh)*(self.H_T_OUT-self.H0_TO_OUT))/(self.H1_TO_OUT - self.H0_TO_OUT)) + self.H0_rh
        print("Humidity after converting from block Data:", hrH)
        return hrH;
        
        
    #Display Humidity Data
    def displayHumidityData(self):
        humidity = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)
        print("Humidity: ")
        print(humidity)

    #Display Magnetometer Data
    def displayMagnetometerData(self):
        orientation = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        print("Orientation: ")
        print(orientation)
    print("------------------------")
    
    #Display Accelerometer Data
    def displayAccelerometerData(self):
        accel_only = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        print("Accelerometer: ")
        print(accel_only)

    #DIsplay Pressure Data
    def displayPressureData(self):
        pressure = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        print("Pressure: ")
        print(pressure)
        


    def run(self):
        while True:
            if self.enableEmulator:
                print("I2C Bus data in Block format")
                self.displayAccelerometerData()
                self.displayMagnetometerData()
                self.displayPressureData()
                self.displayHumidityData()
                print("I2C Bus Data in Readable Format")
                self.current_Humidity = self.humidity()
                self.sensorData.addValue(self.current_Humidity)
                #printing  the sensor data 
                print('-----------------------------------------------')
                print(str(self.sensorData))
                #DeviceDataManager receives SensorData generated 
                self.manager.receive(self.sensorData, self.alertDiff)
                sleep(5)