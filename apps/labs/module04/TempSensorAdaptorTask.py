'''
Created on Feb 7, 2020

@author: HarshithaGS
'''

    
from threading import Thread # importing thread 
from labs.common import SensorData # importing SensorData class to use the attributes of sensor
from labs.common import ConfigUtil # importing ConfigUtil 
from sense_hat import SenseHat #importing the SenseHat Package
from labs.module04 import DeviceDataManager 
from time import sleep
'''
This class  uses  SenseHat class from the sense_hat module in order to read the temperature from the SenseHAT
'''
class TempSensorAdaptorTask(Thread):
    #constructor
    def __init__(self,alertdiff,name):
        '''
        @param enableEmulator: boolean value to start the emulator
        @param sensorData: instantiation of sensorData class
        @param connector: instantiation of smtpClientConnector class
        @param timeInterval: time difference post which new temperature  value is generated
        @param alertDiff: the threshold value for sending alert message and notify user
        @param current_Temp: current value for  temperature      
        '''
        super(TempSensorAdaptorTask, self).__init__() # instance of subclass of Thread class
        Thread.__init__(self) # Overriding the __init__(self [,args]) method to add additional arguments.

        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))

        self.name =name
        self.enableEmulator = True
        self.sensorData = SensorData.SensorData(name)
        self.alertDiff = alertdiff
        self.sensehatLib = SenseHat()
        self.manager = DeviceDataManager.DeviceDataManager()#instance of DeviceDataManager
        

    '''
    This run method will generate a random float variable(Current Value) and sends thisdata to the DeviceDataManager to compare certain conditions in order to send the email notification to user
    '''
    def run (self):
        Thread.run(self)#Overriding the run(self [,args]) method to implement what the thread should do when started.
        while True :
            if self.enableEmulator:
                self.current_Temp = self.sensehatLib.get_temperature()
                self.sensorData.addValue(self.current_Temp)
                    #printing  the sensor data 
                print('-----------------------------------------------')
                print(str(self.sensorData))
                    #DeviceDataManager receives SensorData generated 
                self.manager.receive(self.sensorData, self.alertDiff)
                sleep(5)
            
