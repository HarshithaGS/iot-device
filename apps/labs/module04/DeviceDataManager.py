'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.common import ActuatorData , ConfigUtil
from labs.module04 import MultiActuatorAdaptor 
from labs.module02 import SmtpClientConnector


'''
This class receives the SensorData from the TempSensorAdaptorTask class , HumiditySensorAdpatorTask class ,I2CSensorAdaptorTask class and compares 
if the temperature and humidity respectively is above or  below the nominal temperature. Also, the condition for if the temperature or humidity
exceeds the average is checked. Based on the condition satisfied the respective email is sent to the user
'''
class DeviceDataManager:
    #constructor
    def __init__(self):
        '''
        @param self.actuator       : instance of ActuatorData
        @param self.actuatorAdaptor:  instnace of MultiActuatorAdaptor
        @param self.connector      :  instance of SmtpClientCOnnector
        @param self.nominalTemp    : instance of Nominal Temp set to 20
        @param self.nominalHumidity: instance of Nominal Humidity set to 10
        '''
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))

        self.actuator = ActuatorData.ActuatorData()
        self.actuatorAdaptor = MultiActuatorAdaptor.MultiActuatorAdaptor()
        self.connector = SmtpClientConnector.SmtpClientConnector()
        
        self.nomialTemp = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE,self.config.configConst.NOMINAL_TEMP))
        self.nomialHumidity = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE,self.config.configConst.NOMINAL_HUMIDITY))
        


        
    '''
    This method receives the Temperature/Humidity SensorData and the difference or threshold value
    '''   
    def receive(self,sensor_Data,alertDiff):
        
        #Condition 1 -  If the difference of current temp and nominal temp is greater than the Threshold/alertDiff value 
        if(abs(sensor_Data.getValue() - float(sensor_Data.nominalTemp ))  >= alertDiff):
            self.connector.publishMessage('Alert message for exceeding threshold', str(sensor_Data))
      
        #Condition 2 - If the current temp is less than the nominal temp  then the temp is increased by the difference value . 
        elif(sensor_Data.getValue() < float(sensor_Data.nominalTemp)):
            #set the actuator data as per the current temperature and send a mail
            self.actuator.setCommand(ActuatorData.COMMAND_OFF)
            self.actuator.setStatusCode(ActuatorData.STATUS_ACTIVE)     
            self.actuator.setErrorCode(ActuatorData.ERROR_OK)
            self.actuator.setStatusCode('Increasing ')
            self.actuator.setValue(abs(sensor_Data.getValue()- float(sensor_Data.nominalTemp)))
            self.actuatorAdaptor.sensehatMessage(self.actuator)
            self.connector.publishMessage('Alert message for less than nominal', str(sensor_Data))

        #Condition 3 - If the current temp is greater than the nominal temp  then the temp is decreased by the difference value 
        elif(sensor_Data.getValue() > float(sensor_Data.nominalTemp)):
            #set the actuator data as per the current temperature and send a mail
            self.actuator.setCommand(ActuatorData.COMMAND_ON)
            self.actuator.setStatusCode(ActuatorData.STATUS_ACTIVE)
            self.actuator.setErrorCode(ActuatorData.ERROR_OK)
            self.actuator.setStateData('Decreasing')
            self.actuator.setValue(abs(sensor_Data.getValue() - float(sensor_Data.nominalTemp)))
            self.actuatorAdaptor.sensehatMessage(self.actuator)
            self.connector.publishMessage(' Alert message for more than nominal', str(sensor_Data))




