'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.common import ActuatorData
from sense_hat import SenseHat
from labs.module04 import SenseHatLedActivator

'''
This class processes the actuator data
'''
class MultiActuatorAdaptor():
    
    #constructor
    def __init__(self):
        '''
        Constructor
        @param actuatorData: instance of actuator data class
        @param senseHat: instance of SenseHatLedActivator class   
        '''
        self.actuatorData = ActuatorData.ActuatorData()
        self.senseHat = SenseHatLedActivator.SenseHatLedActivator()
        self.sense = SenseHat()
        
        
    '''
    This method will display the actuator data on the sense hat 
    '''
    def sensehatMessage(self,actuatorData):

        #comparing the actuator data received and actuator instance of this class
        if(self.actuatorData != actuatorData):
            #if the actuator value is greater than zero i.e temperature/Humidity is above nominal temperature/Humidity then show decrease temperature/Humidity  message
            if(actuatorData.getCommand() == 1):
                #the D is represented for Decrease in Temp/Humidity. This is shown on the SenseHat led for better message conveyance
                msg='\n D %.4f:' % (actuatorData.getValue())
                #set color for the SenseHat LED display
                t_colour=[0,0,255]
                b_colour =[255,255,0]
            else:
                #if the actuator value is less than zero i.e temperature/Humidity is below nominal temperature/Humidity then show increase temperature/Humidity  message
#                 the I is represented for Increase in Temp/Humidity
                msg='\n I %.4f:' % (actuatorData.getValue())
                #set color for the SenseHat LED display
                t_colour =[255,255,0]
                b_colour=[0,0,255]
            print("________________________________________")                    
            senseHat = SenseHatLedActivator.SenseHatLedActivator()
            #calls the method to set the respective display message to be shown on sense hat
            senseHat.setDisplayMessage(msg)
            
    
            #Once the sense hat is enabled the message is display else a message is shown that it cannot activate the actuator
            senseHat.enableLed=True
            try:
                senseHat.displayMessage();
                #show the message on the SenseHat Led with the proper color set
                self.sense.show_mesage(msg,text_color=t_colour ,back_color =b_colour)
            except:
                print("Cannot activate the actuator");
            finally:
                senseHat.enableLed=False;
                #Every time new actuator data is generated it is updated to the Actuator Data class
                self.actuatorData.updateData(actuatorData)
        return True