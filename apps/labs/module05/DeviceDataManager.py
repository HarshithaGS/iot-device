
'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.module05 import MultiActuatorAdaptor 
from labs.common import PersistenceUtil , DataUtil


'''
This class reads the data from the Redis and converts from json format to the Actuatorformat and prints on the SenseHat Led
'''
class DeviceDataManager:
    def __init__(self):
        '''
        Constructor
        @param self.actuatorAdaptor:  instnace of MultiActuatorAdaptor
        @param self.persist: instance of PersistenceUtil
        @param self.util :instnace of DataUtil 
        '''
        self.persist=PersistenceUtil.PersistenceUtil();
        self.actuatorAdaptor=MultiActuatorAdaptor.MultiActuatorAdaptor()
        self.util = DataUtil.DataUtil()
        
 
    def ManageData(self):
        i =1
        for i in range(12):
            new_actuator=self.persist.ReadfromRedis();  # Reads actuator Data from the Redis
            actuatordata=self.util.toActuatorDataFromJson(new_actuator)# converts json format to ActuatorData format
            self.actuatorAdaptor.sensehatMessage(actuatordata)
            print(new_actuator) # call the actuator def to see the result on the Sensehat led
            i=i+1
        return i