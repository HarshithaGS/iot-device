'''
Created on Feb 7, 2020

@author: HarshithaGS
'''

'''
This is the main app class
'''
from labs.module05.MultiSensorAdaptor import MultiSensorAdaptor
from labs.module05.DeviceDataManager import DeviceDataManager
from labs.common import PersistenceUtil
import time

'''
DeviceHandlerApp is handling whole application using the instantiation of Adaptor class
'''
class DeviceHandlerApp:
 
    finalkey=PersistenceUtil.PersistenceUtil().ReadfromRedisFlag();
    flag=0
    tempAdp=MultiSensorAdaptor();# instance of MultiSensorAdaptor Class
    flag=1
    print("Waiting for actuator data...")
    time.sleep(60)
    print("Getting actuator data...")
    if(flag==1):
        adp=DeviceDataManager();# instance of DeviceDataManager
        adp.ManageData()