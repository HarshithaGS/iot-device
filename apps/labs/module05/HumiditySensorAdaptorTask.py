'''
Created on Feb 13, 2020

@author: HarshithaGS
'''

from threading import Thread # importing thread 
from time import sleep
from labs.common import SensorData # importing SensorData class to use the attributes of sensor
from sense_hat import SenseHat #importing the SenseHat Package
from labs.common import PersistenceUtil
from labs.common.SensorDataListener import SensorDataListener 
   
 
'''
This class  uses  SenseHat Lib/API from the sense_hat module in order to read the humidity from the SenseHAT
'''        
class HumiditySensorAdaptorTask(Thread):
    #constructor
    def __init__(self,alertdiff , name):
        '''
        @param enableEmulator: boolean value to start the emulator
        @param sensorData: instantiation of sensorData class
        @param connector: instantiation of smtpClientConnector class
        @param timeInterval: time difference post which new temperature  value is generated
        @param alertDiff: the threshold value for sending alert message and notify user
        @param current_Temp: current value for  temperature      
        '''
        super(HumiditySensorAdaptorTask, self).__init__()# instance of subclass of Thread class
        Thread.__init__(self) # Overriding the __init__(self [,args]) method to add additional arguments.
        
        self.enableEmulator = True
        self.sensorData = SensorData.SensorData(name)
        self.alertDiff = alertdiff
        self.sensehatLib = SenseHat()
        self.persistance=PersistenceUtil.PersistenceUtil()
        self.senseListner=SensorDataListener();

    '''
    This run method will generate a random float variable(Current Value) and sends thisdata to the DeviceDataManager to compare certain conditions in order to send the email notification to user
    '''
               
    def run(self):   
        count=0
        while True:
            if(count<=3):
                if self.enableEmulator:
                    self.current_Humidity = self.sensehatLib.get_humidity()
                    self.sensorData.addValue(self.current_Humidity)
                    #printing  the sensor data 
                    print('-----------------------------------------------')
                    print(str(self.sensorData))
                    self.persistance.writeToRedis(self.sensorData , "Humidity" )# Writes data o Redis
                    count+=1
            else:
                self.senseListner.onMessage()
                break;
            sleep(5);           
           
        