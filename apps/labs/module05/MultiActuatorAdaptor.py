'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.common import ActuatorData
from sense_hat import SenseHat
from labs.module05 import SenseHatLedActivator
from labs.common import PersistenceUtil
from labs.common import DataUtil

'''
This class processes the actuator data
'''
class MultiActuatorAdaptor():
    
    #constructor
    def __init__(self):
        '''
        Constructor
        @param actuatorData: instance of actuator data class
        @param senseHat: instance of SenseHatLedActivator class   
        '''
        self.actuatorData = ActuatorData.ActuatorData()
        self.senseHat = SenseHatLedActivator.SenseHatLedActivator()
        self.sense = SenseHat()
        
        self.persist=PersistenceUtil.PersistenceUtil();
        self.util=DataUtil.DataUtil();
        
        
    '''
    This method will display the actuator data on the sense hat 
    '''
    def sensehatMessage(self,actuatorData):

        #comparing the actuator data received and actuator instance of this class
        if(self.actuatorData != actuatorData):
            #if the actuator value is greater than zero i.e temperature/Humidity is above nominal temperature/Humidity then show decrease temperature/Humidity  message
            if(actuatorData.getCommand()==1):
                if(actuatorData.getName()=="Temperature Reading is"):       
                    #the D is represented for Decrease in Temp/Humidity. This is shown on the SenseHat led for better message conveyance
                    msg='\n DT %.4f:' % (actuatorData.curValue)
                    #set color for the SenseHat LED display
                    t_colour=[255, 0, 0]
                    print("Dec Temp")
                elif(actuatorData.getName()=="Humidity Reading is"):
                    msg='\n DH %.4f:' % (actuatorData.curValue)
                    #set color for the SenseHat LED display
                    t_colour=[0,255,0]
                    print("Dec Humidity")
                else:
                    msg='\n DHIC %.4f:' % (actuatorData.curValue)
                    #set color for the SenseHat LED display
                    t_colour=[0,0,255]
                    print("Dec I2C Humidity")
                    
            else:
                if(actuatorData.getName()=="Temperature Reading is"):
                    
                    #the D is represented for Decrease in Temp/Humidity. This is shown on the SenseHat led for better message conveyance
                    msg='\n IT %.4f:' % (actuatorData.curValue)
                    #set color for the SenseHat LED display
                    t_colour=[255, 0, 0]
                    print("Inc Temp")
                elif(actuatorData.getName()=="Humidity Reading is"):
                    msg='\n IH %.4f:' % (actuatorData.curValue)
                    #set color for the SenseHat LED display
                    t_colour=[0,255,0]
                    print("Inc Humidity")
                else:
                    msg='\n IHIC %.4f:' % (actuatorData.curValue)
                    #set color for the SenseHat LED display
                    t_colour=[0,0,255]
                    print("Inc I2C Humidity")
            print("________________________________________")                    
            senseHat = SenseHatLedActivator.SenseHatLedActivator()
            #calls the method to set the respective display message to be shown on sense hat
            senseHat.setDisplayMessage(msg)
            
    
            #Once the sense hat is enabled the message is display else a message is shown that it cannot activate the actuator
            senseHat.setEnableLedFlag('enable');
            try:
                senseHat.displayMessage();
                #show the message on the SenseHat Led with the proper color set
                self.sense.show_mesage(msg,text_color=t_colour)
            except:
                print(" ");
            finally:
                senseHat.enableLed=False;
                #Every time new actuator data is generated it is updated to the Actuator Data class
                self.actuatorData.updateData(actuatorData)
                return True
        return senseHat.enableLed 
    
    
    
    

