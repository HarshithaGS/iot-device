'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.module03 import TempSensorAdaptorTask


'''
This class instantiates the TempSensorAdaptorTask Class 
'''
class TempSensorAdaptor :
    #constructor
    def __init__ (self):
        self.temptask = TempSensorAdaptorTask.TempSensorAdaptorTask(0.5)
        self.adaptorstart(self.temptask)
        
    '''
    This method is to start the thread present in the Task class   
    '''
    def adaptorstart(self, temptask):
        temptask.run();
