'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#import
from sense_hat import SenseHat 
'''
This class Displays message on SenseHat 
'''
class SenseHatLedActivator():
    '''
    @param enableLed: boolean to set the led flag
    @param rateInSec: Thread sleep duration
    @param sh: SensorHat instance
    @param displayMsg: The message to display     
    '''
    enableLed = False
    rateInSec = 1
    rotateDeg = 270
    sh = None
    displayMsg = None

    #constructor
    def __init__(self):
        super(SenseHatLedActivator, self).__init__()
        #check if  sleep duration in greater than zero
        if self.rateInSec > 0:
            # then set the sleep duration
            self.rateInSec = self.rateInSec
        #check if rotation greater than zero
        if self.rotateDeg >= 0:
            #then set the rotation
            self.rotateDeg = self.rotateDeg
            #Sensehat instance is assigned the rotation degree
            self.sh = SenseHat()
            self.sh.set_rotation(self.rotateDeg)
            
            
    def getRateInSeconds(self):
        return self.rateInSec
    
    
    def setEnableLedFlag(self, enable):
        self.sh.clear()
        self.enableLed = enable
        
    #function to set the display message for the SenseHat    
    def setDisplayMessage(self, msg):
        self.displayMsg = msg
        
    #function to show the display message for the SenseHat   . Uses the show message function from the SenseHat Library 
    def displayMessage(self):
        self.sh.show_message(self.displayMsg)
            