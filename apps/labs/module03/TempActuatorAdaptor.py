'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.common import ActuatorData
from labs.module03 import SenseHatLedActivator

'''
This class processes the actuator data
'''
class TempActuatorAdaptor():
    
    #constructor
    def __init__(self):
        '''
        Constructor
        @param actuatorData: instance of actuator data class
        @param senseHat: instance of SenseHatLedActivator class   
        '''
        self.actuatorData = ActuatorData.ActuatorData()
        self.senseHat = SenseHatLedActivator.SenseHatLedActivator()
        
        
        
    '''
    This method will display the actuator data on the sense hat 
    '''
    def sensehatMessage(self,actuatorData):

        #comparing the actuator data received and actuator instance of this class
        if(self.actuatorData != actuatorData):
            #if the actuator value is greater than zero i.e temperature is above nominal temperature then show decrease temperature  message
            if(actuatorData.getCommand() == 1):
                #the D is represented for Decrease in Temp. This is shown on the SenseHat led for better message conveyance
                msg='\n D %.4f:' % (actuatorData.getValue())
            else:
                #if the actuator value is less than zero i.e temperature is below nominal temperature then show increase temperature  message
                # the I is represented for Increase in Temp
                msg='\n I %.4f:' % (actuatorData.getValue())
                                
            senseHat = SenseHatLedActivator.SenseHatLedActivator()
            #calls the method to set the respective display message to be shown on sense hat
            senseHat.setDisplayMessage(msg)
    
            #Once the sense hat is enabled the message is display else a message is shown that it cannot activate the actuator
            senseHat.enableLed=True
            try:
                senseHat.displayMessage();
            except:
                print("Cannot activate the actuator");
            finally:
                senseHat.enableLed=False;
                #Every time new actuator data is generated it is updated to the Actuator Data class
                self.actuatorData.updateData(actuatorData)
                return True