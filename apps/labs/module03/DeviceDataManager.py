'''
Created on Feb 6, 2020

@author: HarshithaGS
'''
#imports
from labs.common import ActuatorData
from labs.module03 import TempActuatorAdaptor
from time import sleep
from labs.common import ConfigUtil 
from labs.module02 import SmtpClientConnector

'''
This class receives the SensorData from the TempSensorAdaptorTask class and compares if the temperature is above and below the nominal temperature. Based on the condition satisfied the respective email is sent to the user
'''
class DeviceDataManager:
    #constructor
    def __init__(self):
        '''
        @param self.actuator:
        @param self.actuatorAdaptor:  
        '''
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        
        self.actuator = ActuatorData.ActuatorData()
        self.actuatorAdaptor = TempActuatorAdaptor.TempActuatorAdaptor()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))
        self.nomialTemp = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE,self.config.configConst.NOMINAL_TEMP))
        self.connector = SmtpClientConnector.SmtpClientConnector()
       
        
    '''
    This method receives the SensorData and the difference or threshold value
    '''   
    def receive(self,sensor_Data,alertDiff):
        
        #Condition 1 -  If the difference of current temp and nominal temp is greater than the Threshold/alertDiff value then an alert mail is sent to the User
        if(abs(sensor_Data.getValue() - float(sensor_Data.nominalTemp ))  >= alertDiff):
            print('\n The Current Temperature exceeds the Average Temperature by'  + str(alertDiff) + ' Hence, triggering the alert message')
            self.connector.publishMessage('Temperature Alert message for exceeding threshold', str(sensor_Data))
      
        #Condition 2 - If the current temp is less than the nominal temp  then the temp is increased by the difference value . This is seen on the SenseHat led display   
        elif(sensor_Data.getValue() < float(sensor_Data.nominalTemp)):
            #set the actuator data as per the current temperature
            self.actuator.setCommand(ActuatorData.COMMAND_OFF)
            self.actuator.setStatusCode(ActuatorData.STATUS_ACTIVE)     
            self.actuator.setErrorCode(ActuatorData.ERROR_OK)
            self.actuator.setStatusCode('Increasing the temperature')
            self.actuator.setValue(abs(sensor_Data.getValue()- float(sensor_Data.nominalTemp)))
            self.actuatorAdaptor.sensehatMessage(self.actuator)


        #Condition 3 - If the current temp is greater than the nominal temp  then the temp is decreased by the difference value . This is seen on the SenseHat led display
        elif(sensor_Data.getValue() > float(sensor_Data.nominalTemp)):
            #set the actuator data as per the current temperature
            self.actuator.setCommand(ActuatorData.COMMAND_ON)
            self.actuator.setStatusCode(ActuatorData.STATUS_ACTIVE)
            self.actuator.setErrorCode(ActuatorData.ERROR_OK)
            self.actuator.setStateData('Decreasing the temperature')
            self.actuator.setValue(abs(sensor_Data.getValue() - float(sensor_Data.nominalTemp)))
            self.actuatorAdaptor.sensehatMessage(self.actuator)

            sleep(self.timeInterval)
