'''
Created on Apr 11, 2020

@author: HarshithaGS
'''

from time import sleep
from sense_hat import SenseHat
import threading

class SenseHatLedActivator(threading.Thread):
    '''
    SenseHatLedActivator : This class is  for LED to show message
    @param enableLed: to enable LED display thread
    @param rateInSec: LED display rate
    @param rotateDeg: angle of display
    @param sh: instance of SenseHat
    @param displayMsg: Message to display
    '''
    enableLed = False
    rateInSec = 1
    rotateDeg = 270
    sh = None
    displayMsg = None
     
     
    def __init__(self, rotateDeg = 270, rateInSec = 1):
        '''
        constructor
        @param rotateDeg: angle of rotation in degree
        @param rateInSec: time in seconds
        '''
        super(SenseHatLedActivator, self).__init__()
        
        if rateInSec > 0:
            self.rateInSec = rateInSec
        if rotateDeg >= 0:
            self.rotateDeg = rotateDeg
            
        self.sh = SenseHat()
        self.sh.set_rotation(self.rotateDeg)
         
         
    def run(self):
        '''
        This function calls the SenseHatLedActivator thread 
        '''
        while True:
            if self.enableLed:
                if self.displayMsg != None:
                    self.sh.show_message(str(self.displayMsg))  #show scrolling LED message
                else:
                    #self.sh.show_letter(str('R'))
                    self.sh.show_letter('')
    
                sleep(self.rateInSec)
                self.sh.clear()

            sleep(self.rateInSec)
     
     
    def getRateInSeconds(self):
        '''
        This function is to get display rate
        @return: 'rateInSec' - time in seconds
        '''
        return self.rateInSec
 
 
    def setEnableLedFlag(self, enable):
        '''
        This function is to enable LED display
        @param enable: True, to set the LED flag else False 
        '''
        self.sh.clear()
        self.enableLed = enable
     
     
    def setDisplayMessage(self, msg):
        '''
        This function is to set the message to be displayed in LED
        @param msg: String message to be displayed
        '''
        self.displayMsg = msg