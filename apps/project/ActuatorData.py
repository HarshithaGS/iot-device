'''
Created on Apr 11, 2020

@author: HarshithaGS
'''

import os
from datetime import datetime

class ActuatorData(object):
    '''
    ActuatorData - class defines the ActuatorData object of Actuator
    @param timeStamp: contains the present date and time 
    @param name: name of actuator data
    @param hasError: boolean to check error
    @param command: type of command
    @param errCode: type of error
    @param statusCode: type of status 
    @param stateData: data of the ActuatorData's state
    @param val: ActuatorData value
    '''

    temperatureactuator = 0
    humidityactuator = 0
    pressureactuator = 0
    

    def __init__(self):
        '''
        ActuatorData Constructor
        '''
        self.updateTimeStamp()

    def updateData(self, data):
        '''
        function to update the ActuatorData
        @param data: ActuatorData 
        '''
        self.temperatureactuator = data.temperatureactuator
        self.humidityactuator = data.humidityactuator
        self.pressureactuator = data.pressureactuator

    
    def updateTimeStamp(self):
        '''
        function to update the date and time 
        '''
        self.timeStamp = str(datetime.now())
        
    def getTemperatureActuator(self):
        '''
        function to get temperature actuator data 
        '''
        print ("tempactuator: "+str(self.temperatureactuator))              
        return self.temperatureactuator
    
    def getHumidityActuator(self): 
        '''
        function to get Humidity actuator data 
        '''   
        print ("humidityactuator: "+str(self.temperatureactuator))                
        return self.humidityactuator
    
    def getPressureActuator(self):
        '''
        function to get Pressure actuator data 
        '''                            
        print ("pressureactuator: "+str(self.temperatureactuator))                
        return self.pressureactuator



    def __str__(self):
        '''
        returns the string representation of the ActuatorData object.
        @return: 'customStr' - ActuatorData object in string.
        '''
        customStr = \
        os.linesep + '\ttimeStamp: ' + self.timeStamp + \
        os.linesep + '\tTemperatureActuator ' + str(self.temperatureactuator) + \
        os.linesep + '\tHumidityActuator: ' + str(self.humidityactuator) + \
        os.linesep + '\tPressureActuator: ' + str(self.pressureactuator)
        return customStr