'''
Created on Apr 11, 2020

@author: HarshithaGS
'''
import logging 
import paho.mqtt.client as mqttClient  # MQTT client
from project import ConfigUtil
from project import SensorData
from project import DataUtil
from project import ActuatorAdaptor
from project.ConfigConst import ConfigConst


class MqttClientConnector(object):
    '''
    MqttClientConnector: This class is used for implementing MQTT protocol connector
    @var port: MQTT port number in integer
    @var brokerAddr: complete address of the host server in String
    @var brokerKeepAlive: to stay active in integer
    @var mqttClient: instance of MQTT client class
    @var config: instance of ConfigUtil class
    @var dataUtil: instance of DataUtil class
    '''
    port = None
    brokerAddr = ""
    brokerKeepAlive = None
    mqttClient = None
    config = None
    dataUtil = None
    ActuatorData=None
    
    def __init__(self):
        '''
        MqttClientConnector Constructor
        '''
        self.createLogger()  # log the console output 
        self.mqttClient = mqttClient.Client()
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        self.brokerAddr= self.config.getProperty(self.config.configConst.MQTT_CLOUD_SECTION, self.config.configConst.CLOUD_MQTT_BROKER)
        self.port = int(self.config.getProperty(self.config.configConst.MQTT_CLOUD_SECTION, self.config.configConst.PORT_KEY))
        self.brokerKeepAlive = int(self.config.getProperty(self.config.configConst.MQTT_CLOUD_SECTION, self.config.configConst.KEEP_ALIVE_KEY))
        self.dataUtil = DataUtil.DataUtil()
        self.sensoData = SensorData.SensorData()
        self.TLS_CERT_PATH = self.config.getProperty(self.config.configConst.UBIDOTS_CLOUD_SECTION , self.config.configConst.CERT_FILE)     
        self.ActuatorAdaptor = ActuatorAdaptor.ActuatorAdaptor()  
             
    def createLogger(self):
        '''
        This Function is used to create logger and File handler
        '''
        # create logger
        self.logger = logging.getLogger('MQTTClientConnector')
        self.logger.setLevel(logging.DEBUG)
        
        handler = logging.FileHandler('Device.log')
        handler.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
        # add formatter to handler
        handler.setFormatter(formatter)
        
        # add handler to logger
        self.logger.addHandler(handler)
      

    
    def connect(self, connectionCallback=None , msgCallback=None):
        '''
        This Function is used to connect to the MQTT host server
        @param connectionCallback: connection call back function
        @param msgCallback: message call back function
        '''
        if(connectionCallback != None):
            self.mqttClient.on_connect = connectionCallback
        else:
            self.mqttClient.on_connect = self.onConnect
            
        if(msgCallback != None) :
            self.mqclient.on_disconnect = msgCallback
        else :
            self.mqttClient.on_disconnect = self.onMessage
            
        self.mqttClient.on_message = self.onMessage    
        
        self.mqttClient.loop_start()
        self.logger.info("Connecting to broker " + self.brokerAddr)
        self.mqttClient.connect(self.brokerAddr, self.port, self.brokerKeepAlive)

        
    def disconnect(self):
        '''
        This Function is used to disconnect from the MQTT host server
        '''
        self.mqttClient.loop_stop()
        self.logger.info("Disconneting the MQTT  broker connection ")
        self.mqttClient.disconnect()
        
    def onConnect(self , client , userData , flags , rc):
        '''
        This Function is  called when the broker responds to our connection request.
        @param flags: flags is a dict that contains response flags from the broker:
        @param rc: The value of rc determines success or not:
                    0: Connection successful
                    1: Connection refused - incorrect protocol version
                    2: Connection refused - invalid client identifier
                    3: Connection refused - server unavailable
                    4: Connection refused - bad username or password
                    5: Connection refused - not authorised
                    6-255: Currently unused.
        '''
        print("On connect RC : " + rc)
        if rc == 0:
            self.logger.info("Connected OK returned Code: " + rc)
        else:
            self.logger.debug("Bad connection Returned Code: " + rc)
            
    def onMessage(self , client , userdata , msg):
        '''
        This Function is called when a message has been received on a topic that the client subscribes to.
        @param msg: MQTTMessage that describes all of the message parameters.
        '''
        rcvdJSON = msg.payload.decode("utf-8")
        self.logger.info("\nReceived Topic is " + msg.topic + " --> \n" + str(rcvdJSON))
        #message is passed to the data util to get updated values of Actuator Data
        newVal=self.dataUtil.updateActuatorData(msg.topic,rcvdJSON)
        # message is passed to Actuator adaptor class to process updated Actuator Data
        self.ActuatorAdaptor.processMessage(self.dataUtil.key, newVal)
        
    def publishMessage(self , topic , msg , qos=2):
        '''
        This function publishes a message on a topic causing a message to be sent to the broker and subsequently from
        the broker to any clients subscribing to matching topics.
        @param topic: The topic that the message should be published on.
        @param msg: The actual message to send. 
        @param qos: The quality of service level to use.
        '''
        if qos < 0 or qos > 2 :
            qos = 2
            
        self.logger.info("\nTopic : "+ str(topic) + "\nMessage :\n" + str(msg))
        self.mqttClient.publish(topic, msg, qos)
        return str(msg)
        
    def publishAndDisconnect(self , topic , msg, qos=2):
        '''
        This Function is used to establish MQTT connection, publish and disconnect
        @param topic: topic of the MQTT message in string
        @param msg: message payload
        @param qos: The quality of service level to use
        '''
        self.logger.info("\nTopic :\n" + str(topic))
        self.connect()
        #while True :
        self.publishMessage(topic, msg, qos)
        self.disconnect()
        
    def subscibetoTopic(self , topic , connnectionCallback=None , qos=2):
        '''
        Function to subscribe the client to one or more topics
        @param topic: topic of the MQTT message in string
        @param connnectionCallback: call back function on subscribe/on message
        @param qos: The quality of service level to use
        '''
        
        self.logger.info('subscibetoTopic : ' + topic)
        if (connnectionCallback != None):
            self.mqttClient.on_subscribe(connnectionCallback)
            self.mqttClient.on_message(connnectionCallback)
        
        self.mqttClient.subscribe(topic , qos)
        self.mqttClient.on_message=self.onMessage
        
    def unsubscibefromTopic(self , topic , connnectionCallback=None):
        '''
        Function to unsubscribe the client from one or more topics.
        @param topic: A single string, or list of strings that are the subscription topics to unsubscribe from.
        @param connnectionCallback: call back function on unsubscribe event
        '''
        if (connnectionCallback != None):
            self.mqttClient.on_unsubscribe(connnectionCallback)
               
        self.mqttClient.unsubscribe(topic)
        
    def subscribeAndDisconnect(self , topic , connnectionCallback=None , qos=2):
        '''
        This Function is used to establish MQTT connection, subscribe the client to one or more topics and disconnect
        @param topic: topic of the MQTT message in string
        @param connnectionCallback: call back function on unsubscribe event
        @param qos: The quality of service level to use
        '''
        self.logger.info("\nTopic :\n" + str(topic))
        self.connect()
        self.subscibetoTopic(topic, connnectionCallback , qos)
        self.disconnect()