'''
Created on Apr 13, 2020

@author: HarshithaGS
'''

#imports
from project import ActuatorData
from project import SenseHatLedActivator
from project import SmtpClientConnector
import logging

class ActuatorAdaptor(object):
    '''
    ActuatorAdaptor - This class processes actuator message, updates and set's the message on the SenseHat
    '''

    ActuatorData = None
    SenseHatLedActivator = None
    
    def __init__(self):
        '''
        ActuatorAdaptor Constructor
        @param self.ActuatorData:  ActuatorData class instance
        @param self.SenseHatLedActivator: SenseHatLedActivator class instance
        @param self.connector : SmtpClientConnector class instance
        '''
        self.createLogger()  # log the console output 
        self.ActuatorData = ActuatorData.ActuatorData()
        self.connector = SmtpClientConnector.SmtpClientConnector()
        self.SenseHatLedActivator = SenseHatLedActivator.SenseHatLedActivator()
        self.SenseHatLedActivator.setEnableLedFlag(True)    #Enabling the SenseHatLedActivator Thread
        self.SenseHatLedActivator.start()   #Starting the SenseHatLedActivator Thread

    def createLogger(self):
        '''
        This Function is used to create logger and File handler
        '''
        # create logger
        self.logger = logging.getLogger('ActuatorAdaptor')
        self.logger.setLevel(logging.DEBUG)
        
        handler = logging.FileHandler('Device.log')
        handler.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
        # add formatter to handler
        handler.setFormatter(formatter)
        
        # add handler to logger
        self.logger.addHandler(handler)
    def processMessage(self, key, pActuatorData):
        '''
        This definition is implemented to process the ActuatorData, update it and set message to SenseHat
        @param pActuatorData: ActuatorData that needs to be processed
        '''
        if (key != None):
            if(self.ActuatorData != pActuatorData):
                if (key == "tempactuator"):
                    self.SenseHatLedActivator.setDisplayMessage('The Current Temperature set to an actuated value of ' + str(pActuatorData.temperatureactuator) + ' C ');
                    self.connector.publishMessage('Alert ! The Temperature range was breached.' , 'Hence setting the temperature to' + str(abs(pActuatorData.temperatureactuator)) + ' C ')
                elif(key == "pressureactuator"):
                    self.SenseHatLedActivator.setDisplayMessage('The Current Pressure set to an actuated value of ' + str(pActuatorData.pressureactuator) + ' Pa ');
                    self.connector.publishMessage('Alert ! The Pressure range was breached.' , 'Hence setting the pressure to ' + str(abs(pActuatorData.pressureactuator)) + ' Pa ')
                elif(key == "humidityactuator"):
                    self.SenseHatLedActivator.setDisplayMessage('The Current Humidity set to an actuated value of' + str(pActuatorData.humidityactuator) + ' RH ');
                    self.connector.publishMessage('Alert ! The Humidity range was breached.' , 'Hence setting the Humidity to' + str(abs(pActuatorData.humidityactuator)) + ' RH ')
        self.ActuatorData.updateData(pActuatorData)
        
        