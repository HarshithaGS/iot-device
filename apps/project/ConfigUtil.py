'''
Created on Apr 11, 2020

@author: HarshithaGS
'''
import configparser
import os
from project import ConfigConst

class ConfigUtil(object):
    '''
    ConfigUtil -     This Class helps in loading , reading and getting properties or values from the default configuration file
    @variable configConst:  ConfigConst instance.
    @variable isLoaded: boolean to check whether configuration file is loaded or not.
    @variable configFilePath: path of configuration file.
    @variable configData: configparser instance.
    '''
    configConst = None
    isLoaded = False
    configFilePath = None
    configData = None


    def __init__(self):
        '''
        ConfigUtil Constructor to initialize.
        '''
        if (self.configData == None):
            self.configData = configparser.ConfigParser()
        if (self.configConst == None):
            self.configConst = ConfigConst.ConfigConst()

        self.configFilePath = self.configConst.DEFAULT_CONFIG_FILE_NAME
    
    
    def isConfigDataLoaded(self):
        '''
        this method checks whether the configuartion file is loaded . Then it  returns the boolean variable isLoaded 
        '''
        return self.isLoaded

    
    def loadConfig(self):
        '''
        This method loads the configuration file and sets the isLoaded variable as true
        First the path is checked to see whether the configuration file exists or not. If yes then the file is read
        '''
        if (os.path.exists(self.configFilePath)):
            self.configData.read(self.configFilePath)
            self.isLoaded = True

            
    def getConfigFile(self):
        '''
        this method returns the configuration file path
        '''
        return self.configFilePath


    def getConfigData(self, forceReload = False):
        '''
        this method returns the configuration data  if it is not loaded or there is a forceReload function set True 
        @return: configuration data
        '''
        if (self.isLoaded == False or forceReload):
            self.loadConfig()
        return self.configData

    
    def getProperty(self, section, key, forceReload = False):
        '''
        this method returns  key from a section of  the configuration file
        '''
        return self.getConfigData(forceReload).get(section, key)    