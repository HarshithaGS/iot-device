'''
Created on Apr 11, 2020

@author: HarshithaGS
'''
from project import MqttClientConnector

UBIDOTS_DEVICE_LABEL = "healthcare/"
UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices/"
QOS = 2

class MqttSubClient(object):
    
    connector = None
    
    def __init__(self):
        '''
        MQTT subscriber constructor, initializes the MQTT client connector.
        @param self.connector : instance of MQTTCLientConnector
        '''
        self.connector = MqttClientConnector.MqttClientConnector()  

    def connect(self):
        '''
        This Function connects to the MQTT broker
        '''
        self.connector.connect()
 
    def disconnect(self):
        '''
        This  Function is used to disconnect to the MQTT broker
        '''
        self.connector.disconnect()
           
    def subscribe(self,topic):
        '''
        This function subscribes the MQTT broker viia the topic name
        '''
        self.connector.subscibetoTopic(topic[0], None , QOS)
        self.connector.subscibetoTopic(topic[1], None , QOS)
        self.connector.subscibetoTopic(topic[2], None , QOS)
        return topic

