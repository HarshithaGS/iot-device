'''
Created on Apr 11, 2020

@author: HarshithaGS
'''

import logging
from project import SensorAdaptor
from datetime import  datetime
import psutil
from time import sleep

'''
SensorAdaptor instance variable
'''
Emulator = None  
'''
singleton instance of the SensorAdaptor
'''
Emulator = SensorAdaptor.SensorAdaptor().getInstance()  
'''
initiating the daemon thread
'''
Emulator.daemon = True  
'''
enabling the thread to begin SensorAdaptor
'''
Emulator.setSensorAdaptor(True)  
'''
setting the logger function for sys performance 
'''
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
logging.info("Starting system performance app daemon thread...")
'''
starting the SensorAdaptor thread
'''
Emulator.start()  

'''
An infinite loop is being created
'''
while(True):
    timeStamp = str(datetime.now())
    cpu=psutil.cpu_percent() #cpu usage percentage is calculated
    mem=psutil.virtual_memory().percent #memory usage percentage is calculated
    sleep(3)
    '''
    File perfdata is used to store output values of sys performance calculations
    '''
    output = open('SystemPerformanceData ','a+')
    print('\n' +timeStamp+"-  CPU Utilization: "+str(cpu) +"; Memory Utilization: "+str(mem))
    output.write('\n' +timeStamp+"-  CPU Utilization: "+str(cpu) +"; Memory Utilization: "+str(mem))
        